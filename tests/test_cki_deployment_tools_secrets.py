"""Tests for the cki.deployment_tools.secrets module."""
import contextlib
import io
import json
import os
import pathlib
import subprocess
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import yaml
import responses

from cki.deployment_tools import secrets


@unittest.skipUnless(b'pbkdf' in subprocess.run(
    ['openssl', 'enc', '-help'], stdout=subprocess.PIPE, stderr=subprocess.STDOUT, check=False
).stdout, 'OpenSSL version too old')
class TestSecrets(unittest.TestCase):
    """Test yaml utils."""

    bar = 'U2FsdGVkX18hbgdS60BhEkxx1selyY0VvHsWawgKtlQ='

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[str]:
        secrets._read_secrets_file.cache_clear()  # pylint: disable=protected-access
        with tempfile.TemporaryDirectory() as directory:
            for name, values in variables.items():
                data = yaml.dump(values) if isinstance(values, dict) else values
                pathlib.Path(directory, name).write_text(data, encoding='utf8')
            yield directory

    def test_variables(self) -> None:
        """Check that basic variable processing works."""
        with self._setup_secrets({
                'internal.yml': {'bar': 0, 'foo': 'qux', 'sec': 'none'},
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            self.assertEqual(secrets.variable('bar'), 0)
            self.assertEqual(secrets.variable('foo'), 'qux')
            self.assertRaises(Exception, secrets.variable, 'baz')

    @mock.patch.dict(os.environ, {
        'ENVPASSWORD': 'unit-tests',
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    @responses.activate
    def test_secrets(self) -> None:
        """Check that basic secret processing works."""
        encrypted = secrets.encrypt('foo')

        responses.get('https://host/v1/apps/data/cki/foo',
                      json={'data': {'data': {'value': 'bar', 'field': 'baz'}}})
        responses.get('https://host/v1/apps/data/cki/qux',
                      json={'data': {'data': {'value': 'bar', 'field': 'baz'}}})

        cases = (
            ('value', 'sec', 'foo', {'sec': {'data': {'value': encrypted}}}),
            ('value missing', 'sec', None, {'sec': {'data': {}}}),
            ('key', 'sec:key', 'foo', {'sec': {'data': {'key': encrypted}}}),
            ('key missing', 'sec:key', None, {'sec': {'data': {}}}),
            ('all', 'sec:', {'value': 'foo', 'key': 'foo'},
             {'sec': {'data': {'value': encrypted, 'key': encrypted}}}),
            ('meta key', 'sec#foo', 'lis', {'sec': {'meta': {'foo': 'lis'}}}),
            ('meta all', 'sec#', {'foo': 'lis'}, {'sec': {'meta': {'foo': 'lis'}}}),
            ('hv default key', 'foo', 'bar', {'foo': {'backend': 'hv'}}),
            ('hv specified key', 'foo:field', 'baz', {'foo': {'backend': 'hv'}}),
            ('hv hash', 'foo:', {'value': 'bar', 'field': 'baz'}, {'foo': {'backend': 'hv'}}),
            ('hv unknown key', 'foo:unknown-field', None, {'foo': {'backend': 'hv'}}),
            ('hv missing meta', 'qux', None, {}),
            ('hv ignored explicit yaml', 'foo', None, {'foo': {'backend': 'yaml'}}),
            ('unknown backend', 'foo', None,
             {'foo': {'backend': 'other', 'data': {'value': encrypted}}}),
        )

        for description, key, expected, data in cases:
            with (
                self.subTest(description),
                self._setup_secrets({'secrets.yml': data}) as directory,
                mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'}),
            ):
                if expected is None:
                    self.assertRaises(Exception, secrets.secret, key)
                else:
                    self.assertEqual(secrets.secret(key), expected)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_filtering(self) -> None:
        """Check that secrets filtering works."""
        cases = (
            ('empty', 'sec[]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('simple', 'sec[active]', ['a'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {'active': False}}),
            ('negated default', 'sec[!active]', ['b'],
             {'a': {'active': True}, 'b': {}}),
            ('multiple', 'sec[active]', ['a', 'b'],
             {'a': {'active': True}, 'b': {'active': True}, 'c': {}}),
            ('multiple conditions', 'sec[active,!revoked]', ['a'],
             {'a': {'active': True}, 'b': {'active': True, 'revoked': True}, 'c': {}}),
            ('meta', 'sec[active]#id', [1],
             {'a': {'active': True, 'id': 1}, 'b': {}}),
        )
        for description, key, expected, metas in cases:
            with self.subTest(description), self._setup_secrets({'secrets.yml': {
                f'sec/{k}': {'data': {'value': secrets.encrypt(k)}, 'meta': v}
                for k, v in metas.items()
            }}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                self.assertEqual(secrets.secret(key), expected)

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    @responses.activate
    def test_edit_secrets(self) -> None:
        """Check that secrets can be edited."""
        encrypted = secrets.encrypt('foo')
        cases = (
            ('single', 'key', 'bar', 'bar',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}},
             {'key': {'backend': 'yaml', 'data': {'value': self.bar}}}),
            ('only backend', 'key', 'bar', 'bar',
             {'key': {'backend': 'yaml'}},
             {'key': {'backend': 'yaml', 'data': {'value': self.bar}}}),
            ('no file', 'key#foo', 'qux', None,
             None,
             {'key': {'meta': {'foo': 'qux'}}}),
            ('no env var', 'key', 'bar', 'bar',
             None,
             None),
            ('key', 'key:bar', 'bar', 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}},
             {'key': {'backend': 'yaml', 'data': {'bar': self.bar, 'value': encrypted}}}),
            ('meta', 'key#foo', 'qux', 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}}),
            ('dict', 'key:', '{value: bar, baz: bar}', 'bar',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}},
             {'key': {'backend': 'yaml', 'data': {'baz': self.bar, 'value': self.bar}}}),
            ('meta dict', 'key#', '{foo: bar, baz: bar}', 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted},
                      'meta': {'baz': 'bar', 'foo': 'bar'}}}),
            ('delete data via empty string', 'key:', '', None,
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'meta': {'foo': 'qux'}}}),
            ('delete data via null string', 'key:', 'null', None,
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'meta': {'foo': 'qux'}}}),
            ('delete data via None', 'key:', None, None,
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'meta': {'foo': 'qux'}}}),
            ('reset data via {}', 'key:', {}, None,
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'data': {}, 'meta': {'foo': 'qux'}}}),
            ('delete meta via empty string', 'key#', None, 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}}),
            ('delete meta via null string', 'key#', 'null', 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}}),
            ('delete meta via None', 'key#', None, 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}}}),
            ('reset meta via {}', 'key#', {}, 'foo',
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {'foo': 'qux'}}},
             {'key': {'backend': 'yaml', 'data': {'value': encrypted}, 'meta': {}}}),
            ('delete all via deleting data', 'key:', '', None,
             {'key': {'data': {'value': encrypted}}},
             {}),
            ('delete all via deleting meta', 'key#', '', None,
             {'key': {'meta': {'foo': 'qux'}}},
             {}),
            ('unknown backend', 'key', 'bar', None,
             {'key': {'backend': 'other'}},
             Exception),
        )
        for description, key, value, expected_value, before, after in cases:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': before,
            } if before is not None else {}) as directory, mock.patch.dict(os.environ, {
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml' if after is not None else '',
            }), mock.patch('cki.deployment_tools.secrets.encrypt',
                           mock.Mock(return_value=self.bar)):
                if after == Exception or after is None:
                    self.assertRaises(Exception, secrets.edit, key, value)
                else:
                    secrets.edit(key, value)
                    if expected_value is not None:
                        self.assertEqual(secrets.secret('key'), expected_value)
                    self.assertEqual(yaml.load(file_path=pathlib.Path(f'{directory}/secrets.yml')),
                                     after)

    @mock.patch.dict(os.environ, {
        'ENVPASSWORD': 'unit-tests',
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    @responses.activate
    def test_edit_secrets_hv(self) -> None:
        """Check that HV secrets can be edited."""
        encrypted = secrets.encrypt('foo')
        cases = (
            ('default', {'key': {'backend': 'hv'}}, 'key', 'bar',
             1, {'other': 'foo', 'value': 'bar'}, {'key': {'backend': 'hv'}}),
            ('field', {'key': {'backend': 'hv'}}, 'key:field', 'bar',
             1, {'other': 'foo', 'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('override', {'key': {'backend': 'hv'}}, 'key:other', 'bar',
             1, {'other': 'bar'}, {'key': {'backend': 'hv'}}),
            ('404', {'key': {'backend': 'hv'}}, 'key', 'bar',
             None, {'value': 'bar'}, {'key': {'backend': 'hv'}}),
            ('replace', {'key': {'backend': 'hv'}}, 'key:', {'field': 'bar'},
             0, {'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('replace string', {'key': {'backend': 'hv'}}, 'key:', "{'field': 'bar'}",
             0, {'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('remove', {'key': {'backend': 'hv'}}, 'key:', '',
             0, None, {}),
            ('missing', {}, 'key', 'bar',
             0, {'value': 'bar'}, {'key': {'backend': 'hv'}}),
            ('implicit migration', {'key': {'data': {'bar': encrypted}}}, 'key', 'bar',
             0, {'bar': 'foo', 'value': 'bar'}, {'key': {'backend': 'hv'}}),
            ('implicit migration field', {'key': {'data': {'bar': encrypted}}}, 'key:field', 'bar',
             0, {'bar': 'foo', 'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('implicit migration all', {'key': {'data': {'bar': encrypted}}}, 'key:',
             {'field': 'bar'}, 0, {'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('implicit migration all string', {'key': {'data': {'bar': encrypted}}}, 'key:',
             "{'field': 'bar'}", 0, {'field': 'bar'}, {'key': {'backend': 'hv'}}),
            ('no explicit migration', {'key': {'backend': 'yaml', 'data': {'bar': encrypted}}},
             'key', 'bar', 0, None,
             {'key': {'backend': 'yaml', 'data': {'value': self.bar, 'bar': encrypted}}}),
        )
        for (description, data, key, value, get_call_count, put_contents, after) in cases:
            with (mock.patch('cki.deployment_tools.secrets.encrypt',
                             mock.Mock(return_value=self.bar)),
                  self.subTest(description),
                  self._setup_secrets({'secrets.yml': data}) as directory,
                  mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'}),
                  mock.patch('cki.deployment_tools.secrets.encrypt',
                             mock.Mock(return_value=self.bar)),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps):
                if get_call_count is None:
                    get_url = rsps.get('https://host/v1/apps/data/cki/key', status=404)
                else:
                    get_url = rsps.get('https://host/v1/apps/data/cki/key',
                                       json={'data': {'data': {'other': 'foo'}}})
                put_url = rsps.put('https://host/v1/apps/data/cki/key')

                secrets.edit(key, value)
                self.assertEqual(yaml.load(file_path=pathlib.Path(f'{directory}/secrets.yml')),
                                 after)
                self.assertEqual(len(get_url.calls),
                                 1 if get_call_count is None else get_call_count)
                if get_call_count:
                    self.assertEqual(get_url.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(len(put_url.calls), 1 if put_contents else 0)
                if put_contents:
                    self.assertEqual(put_url.calls[0].request.headers['X-Vault-Token'], 'token')
                    self.assertEqual(json.loads(put_url.calls[0].request.body)[
                                     'data'], put_contents)

    def test_secrets_invalid(self) -> None:
        """Check that invalid secret files fail."""
        with self._setup_secrets({
                'internal.yml': "['bar', 'baz']",
        }) as directory, mock.patch.dict(os.environ, {
            'CKI_VARS_FILE': f'{directory}/internal.yml',
        }):
            with self.assertRaises(Exception, msg='invalid'):
                secrets.variable('bar')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_secrets_custom(self) -> None:
        """Check that custom secrets files work."""
        with self._setup_secrets({
                'custom-vars.yml': {'ure': 'qux'},
        }) as directory, mock.patch.dict(os.environ, {
            'CUSTOM_VARS_FILE': f'{directory}/custom-vars.yml',
            'CKI_VARS_NAMES': 'CUSTOM_VARS_FILE CUSTOM_VARS_FILE_2',
        }):
            self.assertEqual(secrets.variable('ure'), 'qux')

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_encrypt(self) -> None:
        """Check that basic encryption works."""
        self.assertEqual(secrets.encrypt('bar', salt=False), '0eA1mjLawgSmRg9bpsdPDw==')
        self.assertTrue(secrets.encrypt('bar').startswith('U2FsdGVkX1'))

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    @responses.activate
    def test_validate(self) -> None:
        """Check that HV secrets can be validated."""
        data = {
            'key': {},
            'foo/bar/both': {'backend': 'hv'},
            'foo/meta': {'backend': 'hv'},
        }
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/',
                      json={'data': {'keys': ['foo/', 'hv']}})
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/foo/',
                      json={'data': {'keys': ['bar/']}})
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/foo/bar/',
                      json={'data': {'keys': ['both']}})
        with (self._setup_secrets({'secrets.yml': data}) as directory,
              mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'})):

            missing_hv, missing_meta = secrets.validate()
            self.assertEqual(missing_hv, {'foo/meta'})
            self.assertEqual(missing_meta, {'hv'})

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
    })
    @mock.patch('uuid.uuid4', mock.Mock(return_value='1234'))
    def test_login_oidc(self) -> None:
        """Check that logging in via OIDC works."""
        cases = (
            ('default duration', ['login', '--oidc'], 36000),
            ('custom duration', ['login', '--oidc', '--duration', '2h'], 7200),
        )

        for description, args, expected_duration in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                get_auth_url = rsps.put(
                    'https://host/v1/auth/oidc/oidc/auth_url',
                    json={'data': {'auth_url': 'https://auth_url'}})
                auth_url = rsps.get(
                    'https://auth_url', status=302,
                    headers={'Location': 'http://localhost:8250/oidc/callback?code=567&state=890'})
                callback = rsps.get(
                    'https://host/v1/auth/oidc/oidc/callback?client_nonce=1234&code=567&state=890',
                    json={'auth': {'client_token': 'token'}})
                renew = rsps.put('https://host/v1/auth/token/renew-self')

                secrets.main(args)

                self.assertEqual(json.loads(get_auth_url.calls[0].request.body), {
                    'client_nonce': '1234',
                    'redirect_uri': 'http://localhost:8250/oidc/callback',
                })
                self.assertEqual(len(auth_url.calls), 1)
                self.assertEqual(len(callback.calls), 1)
                self.assertEqual(json.loads(renew.calls[0].request.body), {
                    'increment': expected_duration,
                })
                self.assertEqual(renew.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(secrets.VAULT_TOKEN_PATH.read_text(encoding='utf8'), 'token')

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_APPROLE_SECRET_ID': 'approle-secret-id',
    })
    def test_login_approle(self) -> None:
        """Check that logging in via an approle works."""
        cases = (
            ('default duration', ['--approle', 'somerole'], 36000),
            ('custom duration', ['--approle', 'somerole', '--duration', '2h'], 7200),
        )

        for description, args, expected_duration in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                get_auth_url = rsps.put('https://host/v1/auth/approle/login',
                                        json={'auth': {'client_token': 'token'}})
                renew = rsps.put('https://host/v1/auth/token/renew-self')

                secrets.login_cli(args)

                self.assertEqual(json.loads(get_auth_url.calls[0].request.body), {
                    'role_id': 'somerole',
                    'secret_id': 'approle-secret-id',
                })
                self.assertEqual(json.loads(renew.calls[0].request.body), {
                    'increment': expected_duration,
                })
                self.assertEqual(renew.calls[0].request.headers['X-Vault-Token'], 'token')
                self.assertEqual(secrets.VAULT_TOKEN_PATH.read_text(encoding='utf8'), 'token')

    @mock.patch.dict(os.environ, {
        'VAULT_ADDR': 'https://host',
        'VAULT_APPROLE_SECRET_ID': 'approle-secret-id',
    })
    def test_logout(self) -> None:
        """Check that logging out works."""
        cases = (
            ('token file', 'token', None),
            ('token env', None, 'token'),
            ('wrong token', None, 'wrong token'),
            ('no token', None, None),
        )
        for description, token_file, token_env in cases:
            with (self.subTest(description),
                  responses.RequestsMock(assert_all_requests_are_fired=False) as rsps,
                  mock.patch.dict('os.environ', {'VAULT_TOKEN': token_env} if token_env else {}),
                  tempfile.NamedTemporaryFile() as token, mock.patch(
                      'cki.deployment_tools.secrets.VAULT_TOKEN_PATH', pathlib.Path(token.name))):
                if token_file:
                    secrets.VAULT_TOKEN_PATH.write_text(token_file, encoding='utf8')
                else:
                    secrets.VAULT_TOKEN_PATH.unlink()
                revoke = rsps.put(
                    'https://host/v1/auth/token/revoke-self',
                    status=200 if token_file == 'token' or token_env == 'token' else 400)

                secrets.logout_cli([])

                if token_file or token_env:
                    self.assertEqual(revoke.calls[0].request.headers['X-Vault-Token'],
                                     token_file or token_env)
                else:
                    self.assertEqual(len(revoke.calls), 0)
                self.assertFalse(secrets.VAULT_TOKEN_PATH.exists())

                # make NamedTemporaryFile happy
                secrets.VAULT_TOKEN_PATH.write_text('', encoding='utf8')

    @mock.patch.dict(os.environ, {
        'ENVPASSWORD': 'unit-tests',
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    @responses.activate
    def test_cli(self) -> None:
        """Check that CLI processing works."""
        responses.add('LIST', 'https://host/v1/apps/metadata/cki/', json={'data': {'keys': ['hv']}})
        cases = (
            ('var int', 'variable_cli', ['int'], '0\n'),
            ('var int json', 'variable_cli', ['int', '--json'], '0\n'),
            ('var string', 'variable_cli', ['str'], 'qux\n'),
            ('var string json', 'variable_cli', ['str', '--json'], '"qux"\n'),
            ('var list', 'variable_cli', ['list'], 'a\nb\n'),
            ('var list json', 'variable_cli', ['list', '--json'], '["a", "b"]\n'),
            ('secret', 'secret_cli', ['sec'], 'ure\n'),
            ('secret json', 'secret_cli', ['sec', '--json'], '"ure"\n'),
            ('secret list', 'secret_cli', ['sec[]'], 'ure\n'),
            ('secret list json', 'secret_cli', ['sec[]', '--json'], '["ure"]\n'),
            ('edit secret', 'edit_cli', ['sec', 'int'], ''),
            ('validate', 'validate_cli', [],
             'Missing in HashiCorp Vault: meta\n'
             'Missing in secrets meta data: hv\n'),
            ('main var int', 'main', ['variable', 'int'], '0\n'),
            ('main var int json', 'main', ['variable', 'int', '--json'], '0\n'),
            ('main var string', 'main', ['variable', 'str'], 'qux\n'),
            ('main var string json', 'main', ['variable', 'str', '--json'], '"qux"\n'),
            ('main secret', 'main', ['secret', 'sec'], 'ure\n'),
            ('main edit secret', 'main', ['edit', 'sec', 'int'], ''),
            ('main validate', 'main', ['validate'],
             'Missing in HashiCorp Vault: meta\n'
             'Missing in secrets meta data: hv\n'),
        )
        for description, method, args, expected in cases:
            with self.subTest(description), self._setup_secrets({
                    'internal.yml': {'int': 0, 'str': 'qux', 'list': ['a', 'b']},
                    'secrets.yml': {
                        'sec': {'backend': 'yaml', 'data': {'value': secrets.encrypt('ure')}},
                        'meta': {'backend': 'hv'},
                    },
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/internal.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }):
                with contextlib.redirect_stdout(output := io.StringIO()):
                    getattr(secrets, method)(args)
                self.assertEqual(output.getvalue(), expected)

    @mock.patch.dict(os.environ, {
        'ENVPASSWORD': 'unit-tests',
        'VAULT_ADDR': 'https://host',
        'VAULT_TOKEN': 'token',
    })
    def test_cli_validate(self) -> None:
        """Check that CLI validate processing works."""
        cases = (
            ('matching', ['both'], {'both': {'backend': 'hv'}},
             0, ''),
            ('missing in meta', ['vault'], {},
             1, 'Missing in secrets meta data: vault\n'),
            ('missing in vault', [], {'meta': {'backend': 'hv'}},
             2, 'Missing in HashiCorp Vault: meta\n'),
            ('missing in both', ['vault'], {'meta': {'backend': 'hv'}},
             2, 'Missing in HashiCorp Vault: meta\nMissing in secrets meta data: vault\n'),
        )
        for description, vault, meta, expected_result, expected_output in cases:
            with self.subTest(description), self._setup_secrets({
                    'secrets.yml': meta,
            }) as directory, mock.patch.dict(os.environ, {
                'CKI_VARS_FILE': f'{directory}/internal.yml',
                'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
            }), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                rsps.add('LIST', 'https://host/v1/apps/metadata/cki/',
                         json={'data': {'keys': vault}})
                with contextlib.redirect_stdout(output := io.StringIO()):
                    result = secrets.main(['validate'])
                self.assertEqual(output.getvalue(), expected_output)
                self.assertEqual(result, expected_result)
