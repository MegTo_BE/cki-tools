"""Tests for credential management."""
import contextlib
from http import HTTPStatus
import json
import os
import pathlib
import tempfile
import typing
import unittest
from unittest import mock

from cki_lib import yaml
from freezegun import freeze_time
import responses

from cki.deployment_tools import secrets
from cki_tools.credential_manager import gitlab
from cki_tools.credential_manager import metrics
from cki_tools.credential_manager import utils


class TestCredentialManager(unittest.TestCase):
    """Tests for credential management."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        variables: typing.Dict[str, typing.Any],
    ) -> typing.Iterator:
        with tempfile.TemporaryDirectory() as directory, mock.patch.dict(os.environ, {
            'CKI_SECRETS_FILE': f'{directory}/secrets.yml',
        }):
            pathlib.Path(directory, 'secrets.yml').write_text(yaml.dump(variables), encoding='utf8')
            yield

    def test_all_tokens(self) -> None:
        """Test behavior of all_tokens."""
        cases = (
            ('default', ['t1', 't2'], {'t1'}, ['0']),
            ('multiple', ['t1'], {'t1', 't2'}, ['0']),
        )
        for description, tokens, types, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'meta': {'token_type': t}} for i, t in enumerate(tokens)
            }):
                self.assertEqual(utils.all_tokens(types), expected)

    def test_metrics(self) -> None:
        """Test behavior of metrics.process."""
        cases = (
            ('default', [
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{active="true",deployed="true",name="0"} 9.466848e+08',
            }),
            ('not deployed', [
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01', 'deployed': False},
            ], {
                'cki_token_expires_at{active="true",deployed="false",name="0"} 9.466848e+08',
            }),
            ('not active', [
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01', 'active': False},
            ], {
                'cki_token_expires_at{active="false",deployed="true",name="0"} 9.466848e+08',
            }),
            ('no expires_at', [
                {'token_type': 'gitlab_token'},
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{active="true",deployed="true",name="1"} 9.466848e+08',
            }),
            ('wrong type', [
                {'token_type': 'wrong_type', 'expires_at': '2010-01-01'},
                {'token_type': 'gitlab_token', 'expires_at': '2000-01-01'},
            ], {
                'cki_token_expires_at{active="true",deployed="true",name="1"} 9.466848e+08',
            }),
        )
        for description, metas, expected in cases:
            with self.subTest(description), self._setup_secrets({
                str(i): {'meta': m} for i, m in enumerate(metas)
            }):
                self.assertEqual({
                    m for m in metrics.process().split('\n') if m.startswith('cki_')
                }, expected)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'token'})
    def test_gitlab_create(self) -> None:
        """Test behavior of gitlab.create."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/projects/1/deploy_tokens', json={
            'id': 5, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/groups/4/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.post('https://instance/api/v4/groups/4/deploy_tokens', json={
            'id': 6, 'username': 'user', 'expires_at': '2010-01-01',
            'revoked': False, 'expired': False, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={
            'username': 'user'
        })
        vault_url = responses.put('https://vault/v1/apps/data/cki/token')

        cases = (
            ('project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
                'deployed': True,
            }),
            ('group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, {
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
                'deployed': True,
            }),
            ('project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'scopes': ['scope'],
                'token_name': 'name',
            }, {
                'token_id': 5,
                'created_at': '2000-01-01T00:00:00+00:00',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
                'deployed': True,
            }),
            ('group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'token_name': 'name',
            }, {
                'token_id': 6,
                'created_at': '2000-01-01T00:00:00+00:00',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
                'deployed': True,
            }),
            ('unsupported token type', 'token', {
                'token_type': 'bugzilla_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
            }, None),
            ('unsupported gitlab token type', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
                'scopes': ['scope'],
                'token_name': 'name',
            }, None),
        )
        for description, token, meta, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {'meta': meta}}):
                vault_url.calls.reset()
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('create', token)
                else:
                    gitlab.process('create', token)
                    self.assertEqual(json.loads(vault_url.calls[0].request.body),
                                     {'data': {'value': 'secret'}})
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_gitlab_update(self) -> None:
        """Test behavior of gitlab.update."""
        cases = (
            ('project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
                'token_id': 3,
            }, [{
                'url': 'https://instance/api/v4/projects/1/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                }
            }, {
                'url': 'https://instance/api/v4/projects/1/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',

            }),
            ('group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 3,
            }, [{
                'url': 'https://instance/api/v4/groups/4/access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['scope'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/groups/4/members/2',
                'json': {'username': 'user', 'access_level': 10},
            }], {
                'scopes': ['scope'],
                'access_level': 10,
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
                'scopes': ['api'],
                'token_id': 3,
                'user_id': 2,
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/3',
                'json': {
                    'id': 3,
                    'user_id': 2,
                    'name': 'name',
                    'scopes': ['api'],
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'active': True,
                },
            }, {
                'url': 'https://instance/api/v4/users/2',
                'json': {'username': 'user'},
            }], {
                'scopes': ['api'],
                'token_name': 'name',
                'token_id': 3,
                'created_at': '2000-01-01',
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_id': 2,
                'user_name': 'user',
            }),
            ('project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {
                    'id': 5,
                    'username': 'user',
                    'name': 'name',
                    'scopes': ['scope'],
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'expired': False,
                }
            }], {
                'scopes': ['scope'],
                'token_name': 'name',
                'token_id': 5,
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',

            }),
            ('group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {
                    'id': 6,
                    'username': 'user',
                    'name': 'name',
                    'scopes': ['scope'],
                    'expires_at': '2010-01-01',
                    'revoked': False,
                    'expired': False,
                },
            }], {
                'scopes': ['scope'],
                'token_name': 'name',
                'token_id': 6,
                'expires_at': '2010-01-01',
                'revoked': False,
                'active': True,
                'user_name': 'user',
            }),
            ('runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], {
                'token_id': 5,
                'expires_at': '2020-01-01',
                'active': True,
            }),
            ('unknown token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'some_token',
            }, [], {}),
        )
        for description, token, meta, url_mocks, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {
                    'data': {'value': secrets.encrypt('token')}, 'meta': meta,
            }}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                if expected is None:
                    with self.assertRaises(Exception):
                        gitlab.process('update', token)
                else:
                    gitlab.process('update', token)
                    self.assertEqual(secrets.secret(f'{token}#'), {**meta, **expected})

    @mock.patch.dict(os.environ, {'ENVPASSWORD': 'unit-tests'})
    def test_gitlab_validate(self) -> None:
        """Test behavior of gitlab.validate."""
        cases = (
            ('project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True}
            }], True),
            ('invalid project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True}
            }], False),
            ('inactive project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False}
            }], False),
            ('undeployed project access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
                'deployed': False,
            }, [], True),
            ('group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('undeployed group access token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
                'deployed': False,
            }, [], True),
            ('personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': True},
            }], True),
            ('invalid personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('revoked personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': True, 'active': True},
            }], False),
            ('inactive personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
            }, [{
                'url': 'https://instance/api/v4/personal_access_tokens/self',
                'json': {'revoked': False, 'active': False},
            }], False),
            ('undeployed personal token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'personal_token',
                'instance_url': 'https://instance',
                'deployed': False,
            }, [], True),
            ('project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': False, 'expired': False}
            }], True),
            ('invalid project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'status': HTTPStatus.NOT_FOUND,
            }], False),
            ('revoked project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': True, 'expired': False}
            }], False),
            ('inactive project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
            }, [{
                'url': 'https://instance/api/v4/projects/1/deploy_tokens/5',
                'json': {'revoked': False, 'expired': True}
            }], False),
            ('undeployed project deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'project_deploy_token',
                'project_url': 'https://instance/g/p',
                'token_id': 5,
                'deployed': False,
            }, [], True),
            ('group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': False},
            }], True),
            ('invalid group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'status': HTTPStatus.NOT_FOUND,
            }], False),
            ('revoked group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': True, 'expired': False},
            }], False),
            ('inactive group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
            }, [{
                'url': 'https://instance/api/v4/groups/4/deploy_tokens/6',
                'json': {'revoked': False, 'expired': True},
            }], False),
            ('undeployed group deploy token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'group_deploy_token',
                'group_url': 'https://instance/groups/g',
                'token_id': 6,
                'deployed': False,
            }, [], True),
            ('runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'json': {'id': 5, 'token_expires_at': '2020-01-01'},
            }], True),
            ('invalid runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
            }, [{
                'method': 'POST',
                'url': 'https://instance/api/v4/runners/verify',
                'status': HTTPStatus.UNAUTHORIZED,
            }], False),
            ('undeployed runner authentication token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'runner_authentication_token',
                'instance_url': 'https://instance',
                'deployed': False,
            }, [], True),
            ('unknown token', 'token', {
                'token_type': 'gitlab_token',
                'gitlab_token_type': 'some_token',
            }, [], True),
        )
        for description, token, meta, url_mocks, expected in cases:
            with self.subTest(description), self._setup_secrets({token: {
                    'data': {'value': secrets.encrypt('token')}, 'meta': meta,
            }}), responses.RequestsMock(assert_all_requests_are_fired=False) as rsps:
                for url_mock in url_mocks:
                    rsps.add(**{'method': 'GET', **url_mock})
                rsps.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
                rsps.get('https://instance/api/v4/groups/g', json={'id': 4})
                if not expected:
                    with self.assertRaises(Exception):
                        gitlab.process('validate', token)
                else:
                    gitlab.process('validate', token)

    @freeze_time('2000-01-01T00:00:00.0+00:00')
    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'token'})
    def test_gitlab_rotate_project_group_token(self) -> None:
        """Test behavior of gitlab.rotate for a project or group token."""
        responses.get('https://instance/api/v4/projects/g%2Fp', json={'id': 1})
        responses.post('https://instance/api/v4/projects/1/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/groups/g', json={'id': 4})
        responses.post('https://instance/api/v4/groups/4/access_tokens', json={
            'id': 3, 'user_id': 2, 'created_at': '2000-01-01', 'expires_at': '2010-01-01',
            'revoked': False, 'active': True, 'token': 'secret',
        })
        responses.get('https://instance/api/v4/users/2', json={
            'username': 'user'
        })
        responses.put('https://vault/v1/apps/data/cki/token/946684800')
        responses.put('https://vault/v1/apps/data/cki/othertoken/946684800')

        token_types = (
            ('project token', {
                'gitlab_token_type': 'project_token',
                'project_url': 'https://instance/g/p',
            }),
            ('group token', {
                'gitlab_token_type': 'group_token',
                'group_url': 'https://instance/groups/g',
            }),
        )

        for token_type, token_data in token_types:
            orig_meta = {
                'access_level': 10,
                'deployed': True,
                'expires_at': '2000-01-02T00:00:00.0+00:00',
                'scopes': ['scope'],
                'token_name': 'name',
                'token_type': 'gitlab_token',
                **token_data,
            }
            new_meta = {
                'access_level': 10,
                'active': True,
                'created_at': '2000-01-01',
                'deployed': True,
                'expires_at': '2010-01-01',
                'revoked': False,
                'scopes': ['scope'],
                'token_id': 3,
                'token_name': 'name',
                'token_type': 'gitlab_token',
                'user_id': 2,
                'user_name': 'user',
                **token_data,
            }
            cases = (
                ('default token', [],
                 {'token': {'meta': {**orig_meta}}},
                 {'token': {'meta': {**orig_meta, 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('slash-based token', [],
                 {'token/something': {'meta': {**orig_meta}}},
                 {'token/something': {'meta': {**orig_meta, 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('multiple tokens', [],
                 {'token/something': {'meta': {**orig_meta}},
                  'token/else': {'meta': {**orig_meta}}},
                 {'token/something': {'meta': {**orig_meta, 'deployed': False}},
                  'token/else': {'meta': {**orig_meta, 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('multiple different tokens', [],
                 {'token': {'meta': {**orig_meta}},
                  'othertoken': {'meta': {**orig_meta}}},
                 {'token': {'meta': {**orig_meta, 'deployed': False}},
                  'othertoken': {'meta': {**orig_meta, 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}},
                  'othertoken/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('one of multiple different tokens', ['othertoken'],
                 {'token': {'meta': {**orig_meta}},
                  'othertoken': {'meta': {**orig_meta}}},
                 {'token': {'meta': {**orig_meta}},
                  'othertoken': {'meta': {**orig_meta, 'deployed': False}},
                  'othertoken/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('multiple mixed tokens', [],
                 {'token': {'meta': {**orig_meta}},
                  'token/else': {'meta': {**orig_meta}}},
                 {'token': {'meta': {**orig_meta, 'deployed': False}},
                  'token/else': {'meta': {**orig_meta, 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('non-true deployed field left alone', [],
                 {'token': {'meta': {**orig_meta, 'deployed': None}}},
                 {'token': {'meta': {**orig_meta, 'deployed': None}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
                ('custom fields copied over', [],
                 {'token': {'meta': {**orig_meta, 'bar': 'foo'}}},
                 {'token': {'meta': {**orig_meta, 'bar': 'foo', 'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta, 'bar': 'foo'}}},
                 ),
                ('wrong gitlab token type', [],
                 {'token': {'meta': {**orig_meta, 'gitlab_token_type': 'foo'}}},
                 {'token': {'meta': {**orig_meta, 'gitlab_token_type': 'foo'}}},
                 ),
                ('dry run', ['', True],
                 {'token': {'meta': {**orig_meta}}},
                 {'token': {'meta': {**orig_meta}}},
                 ),
                ('no expiry', [],
                 {'token': {'meta': {**orig_meta, 'expires_at': None}}},
                 {'token': {'meta': {**orig_meta, 'expires_at': None}}},
                 ),
                ('expiry too late not deployed', [],
                 {'token': {'meta': {**orig_meta}},
                  'token/newer': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00',
                                           'deployed': False}}},
                 {'token': {'meta': {**orig_meta}},
                  'token/newer': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00',
                                           'deployed': False}}},

                 ),
                ('expiry too late', [],
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00'}}},
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00'}}},
                 ),
                ('expiry too late, forced but no token', ['', False, True],
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00'}}},
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00'}}},
                 ),
                ('expiry too late but forced', ['token', False, True],
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00'}}},
                 {'token': {'meta': {**orig_meta, 'expires_at': '2000-02-01T00:00:00.0+00:00',
                                     'deployed': False}},
                  'token/946684800': {'backend': 'hv', 'meta': {**new_meta}}},
                 ),
            )

            for description, args, before_secrets, expected_secrets in cases:
                with (self.subTest(description, token_type=token_type),
                      self._setup_secrets(before_secrets)):
                    gitlab.process('rotate', *args)
                    self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                                     expected_secrets)

    @freeze_time('2000-01-03T00:00:00.0+00:00')
    @responses.activate
    @mock.patch.dict(os.environ, {'VAULT_ADDR': 'https://vault', 'VAULT_TOKEN': 'token'})
    def test_gitlab_rotate_personal_token(self) -> None:
        """Test behavior of gitlab.rotate for a personal token."""
        responses.post('https://instance/api/v4/personal_access_tokens/3/rotate',
                       json={'id': 4, 'token': 'new-secret'})
        responses.get('https://instance/api/v4/users/2', json={'username': 'user'})

        responses.get('https://vault/v1/apps/data/cki/token',
                      json={'data': {'data': {'value': 'secret'}}})
        responses.get('https://vault/v1/apps/data/cki/token/B',
                      json={'data': {'data': {'value': 'deployed-secret'}}})
        responses.put('https://vault/v1/apps/data/cki/token/946857600')
        responses.get('https://vault/v1/apps/data/cki/token/946857600',
                      json={'data': {'data': {'value': 'new-secret'}}})

        orig_meta = {
            'active': True,
            'deployed': False,
            'expires_at': '2000-01-02T00:00:00.0+00:00',
            'gitlab_token_type': 'personal_token',
            'instance_url': 'https://instance',
            'scopes': ['api'],
            'token_id': 3,
            'token_type': 'gitlab_token',
            'user_id': 2,
        }
        new_meta = {
            'active': True,
            'created_at': '2000-01-01',
            'deployed': True,
            'expires_at': '2010-01-01',
            'gitlab_token_type': 'personal_token',
            'instance_url': 'https://instance',
            'revoked': False,
            'scopes': ['api'],
            'token_id': 4,
            'token_name': 'name',
            'token_type': 'gitlab_token',
            'user_id': 2,
            'user_name': 'user',
        }
        cases = (
            ('default token', {}, {},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}}},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'active': False, 'revoked': True}},
                'token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
             ),
            ('multiple tokens', {}, {},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}},
                'token/B': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'active': False, 'revoked': True}},
                'token/B': {'backend': 'hv', 'meta': {**orig_meta}},
                'token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
             ),
            ('only deployed', {}, {},
             {'token': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
             {'token': {'backend': 'hv', 'meta': {**orig_meta, 'deployed': True}}},
             ),
            ('new enough', {}, {},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
             ),
            ('new enough but forced', {'force': True, 'token': 'token'}, {},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00'}}},
             {'token': {'backend': 'hv', 'meta': {
                 **orig_meta, 'expires_at': '2000-03-01T00:00:00.0+00:00',
                 'active': False, 'revoked': True}},
              'token/946857600': {'backend': 'hv', 'meta': {**new_meta}}},
             ),
            ('recently used', {}, {'last_used_at': '2000-01-03'},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}}},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}}},
             ),
            ('dry run', {'dry_run': True}, {},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}}},
             {'token': {'backend': 'hv', 'meta': {**orig_meta}}},
             ),
        )

        for description, args, token_attrs, before_secrets, expected_secrets in cases:
            with (self.subTest(description),
                    self._setup_secrets(before_secrets)):
                responses.upsert('GET', 'https://instance/api/v4/personal_access_tokens/3', json={
                    'active': True,
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'last_used_at': '2000-01-01',
                    'id': 3,
                    'name': 'name',
                    'revoked': False,
                    'scopes': ['api'],
                    'user_id': 2,
                    **token_attrs,
                })
                responses.upsert('GET', 'https://instance/api/v4/personal_access_tokens/4', json={
                    'active': True,
                    'created_at': '2000-01-01',
                    'expires_at': '2010-01-01',
                    'last_used_at': '2000-01-01',
                    'id': 4,
                    'name': 'name',
                    'revoked': False,
                    'scopes': ['api'],
                    'user_id': 2,
                    **token_attrs,
                })
                gitlab.process('rotate', **args)
                self.assertEqual(yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE')),
                                 expected_secrets)
