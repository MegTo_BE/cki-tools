[0KRunning with gitlab-runner 15.8.2 (4d1ca121)[0;m
[0K  on wf-aws-aws-internal-a-dm-internal-build oeXrHiJc, system ID: s_8579ead406c8[0;m
section_start:1676885242:resolve_secrets
[0K[0K[36;1mResolving secrets[0;m[0;m
section_end:1676885242:resolve_secrets
[0Ksection_start:1676885242:prepare_executor
[0K[0K[36;1mPreparing the "docker+machine" executor[0;m[0;m
[0KUsing Docker executor with image quay.io/cki/builder-stream9:production ...[0;m
[0KAuthenticating with credentials from $DOCKER_AUTH_CONFIG[0;m
[0KPulling docker image quay.io/cki/builder-stream9:production ...[0;m
[0KUsing docker image sha256:bf9000ff345d93be184ec0f76680877547304a45c7d20ecfe6a7cd2bd34fc31b for quay.io/cki/builder-stream9:production with digest quay.io/cki/builder-stream9@sha256:595d7e9feac3880bbe09cbf26f2022d9fb962ff7d51dbdab56ca998c6a477272 ...[0;m
section_end:1676885521:prepare_executor
[0Ksection_start:1676885521:prepare_script
[0K[0K[36;1mPreparing environment[0;m[0;m
Running on runner-oexrhijc-project-18194050-concurrent-0 via runner-oexrhijc-arr-cki.prod.build.1676885242-301c61cc...
section_end:1676885543:prepare_script
[0Ksection_start:1676885543:get_sources
[0K[0K[36;1mGetting source from Git repository[0;m[0;m
[32;1mSkipping Git repository setup[0;m
[32;1mSkipping Git checkout[0;m
[32;1mSkipping Git submodules setup[0;m
section_end:1676885544:get_sources
[0Ksection_start:1676885544:download_artifacts
[0K[0K[36;1mDownloading artifacts[0;m[0;m
[32;1mDownloading artifacts for merge (3799030687)...[0;m
Downloading artifacts from coordinator... ok      [0;m  id[0;m=3799030687 responseStatus[0;m=200 OK token[0;m=64_NPJwf
[32;1mDownloading artifacts for prepare builder (3799030671)...[0;m
Downloading artifacts from coordinator... ok      [0;m  id[0;m=3799030671 responseStatus[0;m=200 OK token[0;m=64_NPJwf
section_end:1676885546:download_artifacts
[0Ksection_start:1676885546:step_script
[0K[0K[36;1mExecuting "step_script" stage of the job script[0;m[0;m
[0KUsing docker image sha256:bf9000ff345d93be184ec0f76680877547304a45c7d20ecfe6a7cd2bd34fc31b for quay.io/cki/builder-stream9:production with digest quay.io/cki/builder-stream9@sha256:595d7e9feac3880bbe09cbf26f2022d9fb962ff7d51dbdab56ca998c6a477272 ...[0;m
[32;1m$ # # collapsed multi-line command[0;m
[0Ksection_start:1676885547:before_script[collapsed=true]
[0K[36;1mExport needed functions and variables (click to expand)...[0m
[32;1m$ set -euo pipefail[0;m
[32;1m$ # Export general pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export AWS pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export KCIDB pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export functions used only by prepare # collapsed multi-line command[0;m
[32;1m$ # Export variables that we need to build kcidb schema # collapsed multi-line command[0;m
[32;1m$ # Set up the path for locally installed python executables and ccache. # collapsed multi-line command[0;m
[1;32m🗄️ Downloading artifacts from S3 bucket...[0m
Downloading artifacts for '782906595/prepare builder'
  Previous jobs: 3799030671
Downloading artifacts from 782906595/prepare builder/3799030671
Downloading artifacts
Downloading artifacts for '782906595/merge'
  Previous jobs: 3799030687
Downloading artifacts from 782906595/merge/3799030687
Downloading kcidb_all.json file
Completed 3.2 KiB/3.2 KiB (33.3 KiB/s) with 1 file(s) remaining
download: s3://arr-cki-prod-trusted-artifacts/trusted-artifacts/782906595/merge/3799030687/kcidb_all.json to ./kcidb_all.json
Downloading artifacts
[1;32m📦 Extracting CKI pipeline software.[0m
[32;1m$ echo -e "\e[0Ksection_end:$(date +%s):before_script\r\e[0K"[0;m
[0Ksection_end:1676885562:before_script
[0K
[32;1m$ # # collapsed multi-line command[0;m
[0Ksection_start:1676885562:before_info[collapsed=true]
[0K[36;1mGathering information about the pipeline environment (click to expand)...[0m
[32;1m$ # Display information about the pipeline environment # collapsed multi-line command[0;m
🏠 Home directory: /tmp
📦 Container image:
  Image: builder-stream9
  Pipeline: https://gitlab.com/cki-project/containers/-/pipelines/762848494
  Job: https://gitlab.com/cki-project/containers/-/jobs/3689337886
  Merge Request: https://gitlab.com/cki-project/containers/-/merge_requests/456
  Commit: https://gitlab.com/cki-project/containers/-/commit/440cd3123316929b5dba72975a98d97e001d3ddc
  Commit title: Merge: Draft: Add WALinuxAgent-cvm to c9s and fedora builders
  Commit timestamp: 2023-01-31T13:29:34+00:00
🏗️ API URLs:
  Pipeline: https://gitlab.com/api/v4/projects/18194050/pipelines/782906595
  Variables: https://gitlab.com/api/v4/projects/18194050/pipelines/782906595/variables
  Job: https://gitlab.com/api/v4/projects/18194050/jobs/3799030688
💻 CPU and job count:
  CPU count: 16
  Job count: 24
[32;1m$ echo -e "\e[0Ksection_end:$(date +%s):before_info\r\e[0K"[0;m
[0Ksection_end:1676885562:before_info
[0K
[32;1m$ kcidb_build append-dict misc/provenance function="executor" url="${CI_JOB_URL}" service_name="gitlab"[0;m
[32;1m$ # 🛠 Prepare to compile the kernel. # collapsed multi-line command[0;m
[32;1m$ # 🗒 Extract kernel source and get configuration file for tarballs. # collapsed multi-line command[0;m
[32;1m$ # 🛠  Prepare directories to hold binaries and set up macros # collapsed multi-line command[0;m
%_rpmdir /builds/3799030688/workdir/rpms
[32;1m$ # Restore the ccache objects from S3. # collapsed multi-line command[0;m
Restoring ccache...
Summary:
  Hits:              514 / 2705 (19.00 %)
    Direct:          322 / 3715 (8.67 %)
    Preprocessed:    192 / 3356 (5.72 %)
  Misses:           2191
    Direct:         3393
    Preprocessed:   3164
  Uncacheable:     20563
Primary storage:
  Hits:             1215 / 7393 (16.43 %)
  Misses:           6178
  Cache size (GB):  2.73 / 3.00 (91.04 %)
  Cleanups:            3

Use the -v/--verbose option for more details.
Statistics zeroed
[32;1m$ # Print build environment variables for local reproducer runs. # collapsed multi-line command[0;m
[1;33mExported variables:[0m
[1;33m    ARCH=x86_64[0m
[32;1m$ # 📥 Compile the kernel into a tarball. # collapsed multi-line command[0;m
[32;1m$ # 📦 Compile the kernel into an RPM. # collapsed multi-line command[0;m
283421 blocks
[1;33mCompiling the kernel with: rpmbuild --rebuild --target x86_64 --with up --with zfcpdump --without trace --without realtime --without arm64_64k --without debug[0m
Unable to flush stdout: No space left on device
section_end:1676887356:step_script
[0Ksection_start:1676887356:after_script
[0K[0K[36;1mRunning after_script[0;m[0;m
[32;1mRunning after script...[0;m
[32;1m$ # # collapsed multi-line command[0;m
[0Ksection_start:1676887357:after_script[collapsed=true]
[0K[36;1mExport needed functions and variables (click to expand)...[0m
[32;1m$ # Export general pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export AWS pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export KCIDB pipeline functions # collapsed multi-line command[0;m
[32;1m$ # Export variables that we need to build kcidb schema # collapsed multi-line command[0;m
[32;1m$ echo -e "\e[0Ksection_end:$(date +%s):after_script\r\e[0K"[0;m
[0Ksection_end:1676887357:after_script
[0K
[32;1m$ if [[ "${CI_JOB_STATUS}" == "failed" ]]; then # collapsed multi-line command[0;m
🗄️ Deleting artifacts that should not be archived...
🗄️ Generating artifact meta data...
🗄️ Uploading artifacts to S3 bucket...
Uploading kcidb_all.json file
Completed 4.3 KiB/4.3 KiB (24.3 KiB/s) with 1 file(s) remaining
upload: ./kcidb_all.json to s3://arr-cki-prod-trusted-artifacts/trusted-artifacts/782906595/build x86_64/3799030688/kcidb_all.json
Uploading artifacts
🗄️ Generating and uploading static artifact directory list...
Completed 1.4 KiB/1.4 KiB (11.1 KiB/s) with 1 file(s) remaining
upload: ./index.html to s3://arr-cki-prod-trusted-artifacts/trusted-artifacts/782906595/build x86_64/3799030688/index.html
Removing artifacts to prevent GitLab from archiving them
[32;1m$ # Display a couple of relevant links # collapsed multi-line command[0;m
This pipeline uses artifacts stored on S3:
[1;33m https://s3.amazonaws.com/arr-cki-prod-trusted-artifacts/index.html?prefix=trusted-artifacts/782906595/build%20x86_64/3799030688/[0m
More information about this pipeline can be found in DataWarehouse:
[1;33m  https://datawarehouse.cki-project.org/kcidb/builds/redhat:782906595-x86_64-kernel[0m
Check out CKI documentation if you have questions about next steps:
[1;33m  https://cki-project.org/l/devel-faq[0m
section_end:1676887370:after_script
[0Ksection_start:1676887370:upload_artifacts_on_failure
[0K[0K[36;1mUploading artifacts for failed job[0;m[0;m
[32;1mUploading artifacts...[0;m
artifacts: found 1 matching artifact files and directories[0;m 
artifacts-meta.json: found 1 matching artifact files and directories[0;m 
kcidb_all.json: found 1 matching artifact files and directories[0;m 
Uploading artifacts as "archive" to coordinator... 201 Created[0;m  id[0;m=3799030688 responseStatus[0;m=201 Created token[0;m=64_NPJwf
section_end:1676887377:upload_artifacts_on_failure
[0Ksection_start:1676887377:cleanup_file_variables
[0K[0K[36;1mCleaning up project directory and file based variables[0;m[0;m
section_end:1676887380:cleanup_file_variables
[0K[31;1mERROR: Job failed: exit code 1
[0;m
