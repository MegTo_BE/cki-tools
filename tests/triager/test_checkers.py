"""Test checkers."""
from collections import Counter
import re
import unittest
from unittest import mock

from datawarehouse import Datawarehouse
import responses

from cki.triager import cache
from cki.triager import checkers
from cki.triager import compiledregex
from cki.triager import dwobject

from ..utils import mock_attrs

MOCK_REGEXES = [
    {
        "id": 2,
        "issue": {
            "id": 88,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "Bug description",
            "ticket_url": "https://bug.link",
            "resolved": False,
            "generic": False
        },
        "text_match": "bnx2x .* Direct firmware load for",
        "file_name_match": ".*",
        "test_name_match": ".*",
        "testresult_name_match": ".*",
    },
    {
        "id": 15,
        "issue": {
            "id": 126,
            "kind": {
                "id": 1,
                "description": "Kernel bug",
                "tag": "Kernel Bug"
            },
            "description": "BUG: clone.*failing after kernel commit",
            "ticket_url": "http://url.com",
            "resolved": False,
            "generic": False
        },
        "text_match": "tag=kcmp03.*FAIL: clone",
        "file_name_match": "syscalls.fail.log",
        "test_name_match": "LTP",
        "testresult_name_match": ".*",
    }
]


class CheckersTest(unittest.TestCase):
    """Test checkers."""

    def setUp(self):
        """Set up tests."""
        checkers.download.cache_clear()
        compiledregex._get_compiled_issueregexes.cache_clear()  # pylint: disable=protected-access
        cache.get_build.cache_clear()
        cache.get_checkout.cache_clear()
        cache.get_issueregexes.cache_clear()

        responses.get('http://datawarehouse/api/1/kcidb/builds/redhat:952371',
                      json=mock_attrs(checkout_id=1))
        responses.get('http://datawarehouse/api/1/kcidb/checkouts/1', json=mock_attrs())

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_triage(self):
        """Test triage."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})

        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/afoobar', 'name': '8704397_aarch64_2_foobar'},
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
            ]
        })

        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 2)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')
        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 2)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_triage_sorted(self):
        """Test triage correctly sorts URLs."""
        responses.add(responses.GET, url='https://log/a.log', body=b'aaa')
        responses.add(responses.GET, url='https://log/b.log', body=b'bbb')
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [
                          {'id': 2, 'issue': {'id': 88, 'description': 'a'}, 'text_match': 'a'},
                          {'id': 15, 'issue': {'id': 126, 'description': 'b'}, 'text_match': 'b'},
                      ]})

        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', 'origin': 'redhat',
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://log/a.log', 'name': 'a.log'},
                {'url': 'https://log/b.log', 'name': 'b.log'},
            ],
            'misc': {'iid': 1, 'results': [
                {'status': 'FAIL', 'output_files': [
                    {'url': 'https://log/a.log', 'name': 'a.log'},
                    {'url': 'https://log/b.log', 'name': 'b.log'},
                ]},
                {'status': 'FAIL', 'output_files': [
                    {'url': 'https://log/a.log', 'name': 'a.log'},
                    {'url': 'https://log/b.log', 'name': 'b.log'},
                ]},
            ]},
        })

        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual([m.log_file.url for m in issues.matches],
                         ['https://log/a.log'] * 10 + ['https://log/b.log'] * 10)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_logfile_redirects(self):
        """Test triage works as expected on HTTP redirects."""
        redirecting_url = "https://logs/foobar"
        file_url = "https://real-logs/foobar"

        responses.add(responses.GET, redirecting_url, status=301, headers={"Location": file_url})
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [{'url': redirecting_url, 'name': 'foobar'}]
        })

        cases = (
            ('default', 200, 'text/plain', checkers.MatchStatus.FULL_MATCH),
            ('truncated still matching', 65, 'text/plain', checkers.MatchStatus.FULL_MATCH),
            ('truncated too short', 50, 'text/plain', checkers.MatchStatus.NO_MATCH),
            ('binary', 200, 'application/binary', checkers.MatchStatus.FULL_MATCH),
        )

        for description, length, content_type, expected_match in cases:
            with (self.subTest(description),
                  mock.patch('cki.triager.settings.MAX_CONTENT_LENGTH', length)):
                responses.upsert(responses.GET, url=file_url, content_type=content_type, body=(
                    b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for bfq_bfqq_move '))
                responses.calls.reset()
                checkers.download.cache_clear()
                issues = checkers.triage(test, [])
                responses.assert_call_count(redirecting_url, 1)
                responses.assert_call_count(file_url, 1)
                self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
                self.assertEqual(len(issues.matches), 1)
                self.assertEqual(issues.matches[0].status, expected_match)
                self.assertEqual(issues.matches[0].regex.issue_id, 88)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_multiple_matches(self):
        """Test triage. More than one regex matches."""
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=(b'[   41.451946] tag=kcmp03 error error FAIL: clone'))

        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'ltp',
            'comment': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log',
                 'name': '8704397_aarch64_2_systemd_journal.log'},
                {'url': 'https://logs/syscalls.fail.log',
                 'name': '8704397_aarch64_2_syscalls.fail.log'},
            ]
        })

        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 3)
        self.assertEqual(issues.matches[0].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[0].regex.issue_id, 88)
        self.assertEqual(issues.matches[1].status, checkers.MatchStatus.NO_MATCH)
        self.assertEqual(issues.matches[1].regex.issue_id, 88)
        self.assertEqual(issues.matches[2].status, checkers.MatchStatus.FULL_MATCH)
        self.assertEqual(issues.matches[2].regex.issue_id, 126)

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    @mock.patch('cki.triager.checkers.search_url', wraps=checkers.search_url)
    def test_triage_subtests_is_cheap(self, wrapped_search_url):
        """Test that triaging a test with subtests will download and match each file once."""
        subtest_count = 20

        results = []
        results_urls = []
        for iid in range(subtest_count):
            resultoutput_url = f'https://logs/resultoutput_{iid}.log'
            results_urls.append(resultoutput_url)
            responses.add(responses.GET, url=resultoutput_url,
                          body=b'No match in here')
            results.append({
                'comment': f'redhat:113990600.{iid}', 'name': f'redhat:113990600.{iid}',
                'status': 'FAIL',
                'output_files': [
                    {'name': 'resultoutput.log', 'url': resultoutput_url}
                ],
            })

        responses.add(responses.GET, url='https://logs/console.log', body=(b'No match in here'))
        responses.add(responses.GET, url='https://logs/syscalls.fail.log',
                      body=b'[   41.451946] tag=kcmp03 error error FAIL: clone')
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': MOCK_REGEXES})

        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat",
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'ltp',
            'comment': 'LTP',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/console.log', 'name': 'console.log'},
                {'url': 'https://logs/syscalls.fail.log', 'name': 'syscalls.fail.log'},
            ],
            'misc': {
                "iid": 1,
                'results': results,
            }
        })

        triage_result = checkers.triage(test, [])
        self.assertEqual(triage_result.status, checkers.TriageStatus.SUCCESS)
        # "2 test files" are used by the test itself and all its results: 2*(1 + 20) = 42 log_files
        # "1 subtest file" is used by each result: 20 log_files. total = 62 log_files
        # The first regex has no file_name_match, test_name_match, testresult_name_match;
        # therefore it is applicable for all 62 log_files,
        # however, it will never match their text, resulting in 62 "NO_MATCH"
        # The second regex matches only the log_files for the test file named "syscalls.fail.log";
        # therefore its applicable for 1 test + 20 subtests = 21 log_files,
        # and the mocked content should match everytime, resulting in 21 "FULL_MATCH"
        self.assertEqual(Counter((m.regex.id, m.status) for m in triage_result.matches), {
            (2, checkers.MatchStatus.NO_MATCH): 62,
            (15, checkers.MatchStatus.FULL_MATCH): 21,
        })

        # Should GET once
        responses.assert_call_count('https://logs/console.log', 1)
        responses.assert_call_count('https://logs/syscalls.fail.log', 1)
        for iid in range(subtest_count):
            responses.assert_call_count(f'https://logs/resultoutput_{iid}.log', 1)

        # Should do the regex matching once
        regex_2 = re.compile(MOCK_REGEXES[0]["text_match"])
        regex_15 = re.compile(MOCK_REGEXES[1]["text_match"])
        wrapped_search_url.assert_has_calls([
            mock.call('https://logs/console.log', 2, regex_2),
            *[mock.call(url, 2, regex_2) for url in sorted(results_urls)],
            mock.call('https://logs/syscalls.fail.log', 2, regex_2),
            mock.call('https://logs/syscalls.fail.log', 15, regex_15),
        ])

    @responses.activate
    @mock.patch('cki.triager.settings.DW_CLIENT', Datawarehouse('http://datawarehouse'))
    def test_lazy(self):
        """Test triage doesnt download file if not necessary."""
        responses.add(responses.GET, 'http://datawarehouse/api/1/issue/-/regex',
                      json={'results': [MOCK_REGEXES[1]]})  # This regex has test constraints
        test = dwobject.from_attrs('test', {
            'id': 'redhat:113990600', "origin": "redhat", "misc": {"iid": 1},
            'status': 'FAIL',
            'build_id': 'redhat:952371',
            'path': 'boot',
            'comment': 'Boot test',
            'duration': 0,
            'output_files': [
                {'url': 'https://logs/foobar', 'name': '8704397_aarch64_2_foobar'},
            ]
        })

        # check does not fail to find the mocked URL
        issues = checkers.triage(test, [])
        self.assertEqual(issues.status, checkers.TriageStatus.SUCCESS)
        self.assertEqual(len(issues.matches), 0)
