"""Datawarehouse UMB Submitter tests."""
import copy
from http import HTTPStatus
from importlib import resources
import json
import os
import unittest
from unittest import mock

import pika
import requests
from requests.exceptions import HTTPError
import responses

from cki.kcidb import datawarehouse_umb_submitter

from . import assets

DW_API = 'http://server'

CHECKOUT = {
    "id": "redhat:1163293372",
    "origin": "redhat",
    "valid": True,
}

EMPTY_BODY = {
    "version": {"major": 4, "minor": 2},
    "checkouts": [],
    "builds": [],
    "tests": [],
}

CALLBACK_KWARGS = {
    "body": EMPTY_BODY,
    "routing_key": "routing_key",
    "headers": None,
    "ack_fn": None,
    "timeout": False,
}

DATAWAREHOUSE_ENVIRON = {
    "DATAWAREHOUSE_URL": DW_API,
    "DATAWAREHOUSE_TOKEN_SUBMITTER": "datawarehouse_token",
}


class TestDatawarehouseUMBSubmitter(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_umb_submitter."""

    @mock.patch.object(datawarehouse_umb_submitter.Receiver, "callback")
    @mock.patch('pika.BlockingConnection')
    @mock.patch.dict(
        os.environ,
        {
            "RABBITMQ_HOST": "host",
            "RABBITMQ_PORT": "123",
            "RABBITMQ_USER": "user",
            "RABBITMQ_PASSWORD": "password",
            "RABBITMQ_QUEUE": "queue",
            "RABBITMQ_ROUTING_KEYS": "routing1 routing2",
            **DATAWAREHOUSE_ENVIRON,
        },
    )
    def test_main(self, connection, callback):
        connection().channel().consume.return_value = [
            (
                mock.Mock(routing_key="routing_key", delivery_tag="delivery_tag"),
                pika.BasicProperties(),
                json.dumps(EMPTY_BODY),
            )
        ]

        datawarehouse_umb_submitter.main()
        callback.assert_called_with(**CALLBACK_KWARGS)

    def test_batched(self):
        with (
            self.subTest("Bad batch size: 0"),
            self.assertRaisesRegex(ValueError, r"n must be at least one"),
        ):
            next(datawarehouse_umb_submitter.batched([1, 2, 3, 4], 0))

        batch_size = 3
        cases = {
            "Empty data": ([], []),
            "Data smaller than batch size": ([1, 2], [(1, 2)]),
            "Data same size than batch size": ([1, 2, 3], [(1, 2, 3)]),
            "Data bigger than batch size": ([1, 2, 3, 4, 5, 6, 7], [(1, 2, 3), (4, 5, 6), (7,)]),
        }
        for description, (data, expected) in cases.items():
            with self.subTest(description):
                result = list(datawarehouse_umb_submitter.batched(data, batch_size))
                self.assertEqual(result, expected)

    @responses.activate
    @mock.patch.dict(os.environ, DATAWAREHOUSE_ENVIRON)
    @mock.patch.object(datawarehouse_umb_submitter, "BATCH_SIZE", 4)
    def test_callback_empty_lists(self):
        """Ensure callback does not submit payloads consisting of empty lists."""
        create_request = responses.post(DW_API + "/api/1/kcidb/submit")

        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)

        receiver = datawarehouse_umb_submitter.Receiver()

        with self.subTest("All lists are empty -> No requests"):
            callback_kwargs["body"] = copy.deepcopy(EMPTY_BODY)
            receiver.callback(**callback_kwargs)
            self.assertEqual(create_request.call_count, 0, "Should not submit empty lists")

    @classmethod
    def _load_kcidb_file(cls):
        path_to_file = resources.files(assets) / "kcidb_all.json"
        with path_to_file.open(encoding="utf-8") as f:
            return json.load(f)

    @responses.activate
    @mock.patch.dict(os.environ, DATAWAREHOUSE_ENVIRON)
    @mock.patch.object(datawarehouse_umb_submitter, "BATCH_SIZE", 4)
    def test_callback_huge_message(self):
        """Ensure callback will split huge messages into reasonable payloads."""
        create_request = responses.post(DW_API + "/api/1/kcidb/submit")

        # Prepare body with tests able to extrapolate the overridden BATCH_SIZE
        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        full_body = self._load_kcidb_file()
        callback_kwargs["body"] = full_body

        receiver = datawarehouse_umb_submitter.Receiver()
        receiver.callback(**callback_kwargs)

        self.assertEqual(
            create_request.call_count,
            4,
            (
                "There should be at least one call for object type,"
                " plus one for each list extrapolating the batch size"
            ),
        )

        combined_submitted_body = copy.deepcopy(EMPTY_BODY)
        for call in responses.calls:
            body = json.loads(call.request.body)
            self.assertEqual(combined_submitted_body["version"], body["data"]["version"])
            for key in ["checkouts", "builds", "tests"]:
                combined_submitted_body[key].extend(body["data"].get(key, []))

        for key in ["checkouts", "builds", "tests"]:
            self.assertCountEqual(
                combined_submitted_body[key],
                full_body[key],
                f"Expected {create_request.url!r} to be called with {full_body[key]!r}",
            )

        create_request.calls.reset()

        partial_body = {"version": full_body["version"], "tests": full_body["tests"]}
        partial_body_callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        partial_body_callback_kwargs["body"] = partial_body
        with self.subTest("Submits partial bodies, skipping empty lists"):
            receiver.callback(**partial_body_callback_kwargs)

            self.assertEqual(create_request.call_count, 2, "Should submit twice with tests only")

            combined_submitted_body = copy.deepcopy(EMPTY_BODY)
            for call in responses.calls:
                body = json.loads(call.request.body)
                self.assertEqual(combined_submitted_body["version"], body["data"]["version"])
                for key in ["checkouts", "builds", "tests"]:
                    combined_submitted_body[key].extend(body["data"].get(key, []))

            self.assertCountEqual(
                combined_submitted_body,
                partial_body,
                f"Expected {create_request.url!r} to be called with {partial_body!r}",
            )

    @responses.activate
    @mock.patch.dict(os.environ, DATAWAREHOUSE_ENVIRON)
    def test_callback_valid(self):
        """Test callback when passed valid KCIDB data."""
        receiver = datawarehouse_umb_submitter.Receiver()
        callback_kwargs = copy.deepcopy(CALLBACK_KWARGS)
        body = self._load_kcidb_file()
        callback_kwargs["body"] = body

        with self.subTest("Validate successful calls to submit work as expected"):
            create_request = responses.post(DW_API + '/api/1/kcidb/submit')
            receiver.callback(**callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {EMPTY_BODY!r}",
            )

        with self.subTest("Validate BAD REQUEST calls to submit work as expected"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_REQUEST)

            with self.assertLogs(datawarehouse_umb_submitter.LOGGER, level="ERROR") as log_ctx:
                receiver.callback(**callback_kwargs)

            expected_log = (
                f"ERROR:{datawarehouse_umb_submitter.LOGGER.name}:"
                "Failed to submit data from UMB to DataWarehouse"
            )
            self.assertTrue(log_ctx.output[0].startswith(expected_log),
                            f"Expected {log_ctx.output[0]=!r} to start with {expected_log!r}")

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {body!r}",
            )

        with self.subTest("Validate any other HTTP error raises"):
            responses.calls.reset()
            create_request = responses.post(DW_API + '/api/1/kcidb/submit',
                                            status=HTTPStatus.BAD_GATEWAY)

            with self.assertRaises(HTTPError):
                receiver.callback(**callback_kwargs)

            responses.assert_call_count(create_request.url, 1)
            request: requests.PreparedRequest = responses.calls[0].request
            self.assertEqual(
                request.body,
                json.dumps({"data": body}),
                f"Expected {create_request.url!r} to be called with {body!r}",
            )

    @mock.patch.dict(os.environ, DATAWAREHOUSE_ENVIRON)
    def test_callback_invalid(self):
        """Test callback when passed invalid KCIDB data."""
        receiver = datawarehouse_umb_submitter.Receiver()
        with (
            mock.patch.object(receiver.dw_api.kcidb.submit, "create") as mock_create,
            self.assertLogs(datawarehouse_umb_submitter.LOGGER, level="ERROR") as log_ctx,
        ):
            receiver.callback(body={})
            self.assertFalse(mock_create.called)
        self.assertTrue(
            log_ctx.output[0].startswith(
                f"ERROR:{datawarehouse_umb_submitter.LOGGER.name}:"
                "Failed to validate data from UMB\nTraceback"
            )
        )
