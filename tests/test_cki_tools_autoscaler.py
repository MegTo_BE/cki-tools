"""Test cki_tools.autoscaler."""
import unittest
from unittest import mock

from cki_tools.autoscaler import Application


@mock.patch('kubernetes.config.load_incluster_config', mock.Mock())
@mock.patch('kubernetes.dynamic.client.DynamicClient', mock.Mock())
class TestPodsAutoscalerEvaluate(unittest.TestCase):
    """Test PodsAutoscaler."""

    def test_scale(self):
        """Test basic scaling."""
        app = Application('test_app', {
            'messages_per_replica': 20,
            'max_scale_up_step': 2,
            'max_scale_down_step': 2,
            'replicas_min': 1,
            'replicas_max': 5,
            'message_baseline': 0,
            'queue': 'test_queue',
            'deployment_config': 'test_dc',
        }, None)
        cases = (
            ('no messages', 0, 1, 1),
            ('below messages_per_replica', 19, 1, 1),
            ('exactly messages_per_replica', 20, 1, 1),
            ('scale up a bit more', 21, 1, 2),
            ('scale up a lot more', 41, 1, 3),
            ('max_scale_up_step', 200, 1, 3),
            ('max_scale_up_step', 200, 3, 5),
            ('scale up replicas_max', 2000, 4, 5),
            ('at replicas_max', 2000, 5, 5),
            ('max_scale_down_step', 0, 5, 3),
            ('replica_min', 0, 3, 1),
            ('replica_min', 0, 2, 1),
            ('scale down a lot', 19, 3, 1),
            ('scale down a bit', 39, 3, 2),
            ('at messages_per_replica', 40, 2, 2),
            ('below messages_per_replica', 39, 2, 2),
        )

        for description, messages, replicas, expected_replicas in cases:
            with self.subTest(description):
                self.assertEqual(app.evaluate(messages, replicas), expected_replicas)

    def test_baseline(self):
        """Test scaling to zero."""
        app = Application('test_app', {
            'messages_per_replica': 100,
            'max_scale_up_step': 10,
            'max_scale_down_step': 10,
            'replicas_min': 0,
            'replicas_max': 10,
            'message_baseline': 500,
            'queue': 'test_queue',
            'deployment_config': 'test_dc',
        }, None)
        cases = (
            ('no messages', 0, 1, 0),
            ('at baseline', 500, 1, 0),
            ('above baseline', 501, 0, 1),
            ('halfway', 1000, 0, 5),
            ('max', 1500, 0, 10),
            ('above max', 1500, 5, 10),
            ('scaling down', 1000, 10, 5),
            ('scaling to zero', 500, 5, 0),
            ('scaling below baseline', 0, 5, 0),
        )

        for description, messages, replicas, expected_replicas in cases:
            with self.subTest(description):
                self.assertEqual(app.evaluate(messages, replicas), expected_replicas)
