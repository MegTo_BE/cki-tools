"""KCIDB utils tests."""
import unittest
from unittest import mock

import datawarehouse
import responses

from cki.kcidb import utils

CHECKOUT_ID = "redhat:123"
CHECKOUT_IID = "1"


class TestUtils(unittest.TestCase):
    """Test Job methods."""

    @responses.activate
    def test_patchset_hash(self):
        """Test patchset_hash return values."""
        responses.add(responses.GET, 'https://s/p_1', body=b'p_1')
        responses.add(responses.GET, 'https://s/p_2/', body=b'p_2')

        patch_list = [
            'https://s/p_1', 'https://s/p_2/'
        ]

        self.assertEqual('', utils.patch_list_hash([]))

        self.assertEqual(
            'a560b2642c43bb3494b79d364d9371dd4468307443d1fde7b12f51e7ba4c1b33',
            utils.patch_list_hash([patch_list[0]])
        )
        self.assertEqual(
            '8d2e6de35099fc8e1c9f98f6c107c004f414846d30f6315a23ccbe0d9a664328',
            utils.patch_list_hash(patch_list)
        )

    @responses.activate
    @mock.patch.dict('os.environ', {'DATAWAREHOUSE_URL': 'https://host.url'})
    def test_unknown_issues(self):
        """Verify unknown issues are filtered correctly."""
        dw = datawarehouse.Datawarehouse("https://host.url")
        checkout_url = f"https://host.url/api/1/kcidb/checkouts/{CHECKOUT_ID}"
        checkout_all_url = f"{checkout_url}/all"

        responses.get(checkout_url, json={"id": CHECKOUT_ID, "misc": {"iid": CHECKOUT_IID}})

        cases = (
            ('fail', {
             'tests': [{'id': 1, 'status': 'FAIL', 'waived': False}],
             }, 1),
            ('pass', {
             'tests': [{'id': 1, 'status': 'PASS', 'waived': False}],
             }, 0),
            ('error', {
             'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
             }, 1),
            ('waived fail', {
             'tests': [{'id': 1, 'status': 'FAIL', 'waived': True}],
             }, 0),
            ('waived error', {
             'tests': [{'id': 1, 'status': 'ERROR', 'waived': True}],
             }, 0),
            ('triaged test fail', {
                'tests': [{'id': 1, 'status': 'FAIL', 'waived': False}],
                'issueoccurrences': [{'is_regression': False, 'test_id': 1}],
            }, 0),
            ('triaged test error', {
                'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
                'issueoccurrences': [{'is_regression': False, 'test_id': 1}],
            }, 0),
            ('triaged test fail with testresult', {  # This should fail after DW#321
                'tests': [{'id': 1, 'status': 'FAIL', 'waived': False}],
                'testresults': [{'id': 2, 'test_id': 1, 'status': 'FAIL'}],
                'issueoccurrences': [{'is_regression': False, 'test_id': 1}],
            }, 0),
            ('triaged test error with testresult', {  # This should fail after DW#321
                'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
                'testresults': [{'id': 2, 'test_id': 1, 'status': 'ERROR'}],
                'issueoccurrences': [{'is_regression': False, 'test_id': 1}],
            }, 0),
            ("triaged testresult fail", {
                "tests": [{"id": 1, "status": "FAIL", "waived": False}],
                "testresults": [
                    {'id': 2, 'test_id': 1, 'status': 'FAIL'},
                    {'id': 3, 'test_id': 1, 'status': 'FAIL'},  # This should fail after DW#321
                ],
                "issueoccurrences": [{"is_regression": False, "testresult_id": 2}],
            }, 0),
            ('triaged testresult error', {
                'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
                'testresults': [
                    {'id': 2, 'test_id': 1, 'status': 'ERROR'},
                    {'id': 3, 'test_id': 1, 'status': 'ERROR'},  # This should fail after DW#321
                ],
                'issueoccurrences': [{'is_regression': False, 'testresult_id': 2}],
            }, 0),
            ('regression fail', {
                'tests': [{'id': 1, 'status': 'FAIL', 'waived': False}],
                'issueoccurrences': [{'is_regression': True, 'test_id': 1}],
            }, 1),
            ('regression error', {
                'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
                'issueoccurrences': [{'is_regression': True, 'test_id': 1}],
            }, 1),
            ('regression testresult fail', {
                'tests': [{'id': 1, 'status': 'FAIL', 'waived': False}],
                'testresults': [{'id': 2, 'test_id': 1, 'status': 'ERROR'}],
                'issueoccurrences': [
                    {'is_regression': False, 'test_id': 1},  # Triaging directly doesn't override
                    {'is_regression': True, 'testresult_id': 2}  # the testresult regressions
                ],
            }, 1),
            ('regression testresult error', {
                'tests': [{'id': 1, 'status': 'ERROR', 'waived': False}],
                'testresults': [{'id': 2, 'test_id': 1, 'status': 'ERROR'}],
                'issueoccurrences': [
                    {'is_regression': False, 'test_id': 1},  # Triaging directly doesn't override
                    {'is_regression': True, 'testresult_id': 2}  # the testresult regressions
                ],
            }, 1),
        )

        for description, data, expected in cases:
            with self.subTest(description):
                responses.upsert(responses.GET, checkout_all_url, json=data)
                dw_all = dw.kcidb.checkouts.get(id=CHECKOUT_ID).all.get()

                result = utils.unknown_issues(dw_all)
                self.assertEqual(len(result), expected, f"{result=}")

        with self.subTest("Passing the optional tests parameter reflects on the outcome"):
            data = {
                "tests": [
                    {"id": 1, "status": "FAIL", "build_id": "redhat:1"},
                    {"id": 2, "status": "PASS", "build_id": "redhat:1"},
                    {"id": 3, "status": "FAIL", "build_id": "redhat:2"},
                    {"id": 4, "status": "PASS", "build_id": "redhat:2"},
                ]
            }
            responses.upsert(responses.GET, checkout_all_url, json=data)
            dw_all = dw.kcidb.checkouts.get(id=CHECKOUT_ID).all.get()

            result = utils.unknown_issues(dw_all)
            self.assertEqual({t.id for t in result}, {1, 3}, "Payload should return both FAIL")

            filtered_tests = (t for t in dw_all.tests if t.build_id == "redhat:1")
            filtered_result = utils.unknown_issues(dw_all, tests=filtered_tests)
            self.assertEqual({t.id for t in filtered_result}, {1})
