---
title: cki_tools.autoscaler
linkTitle: autoscaler
description: Autoscaler to scale replicas up and down
---

Dynamically scale up and down the replicas of a DeploymentConfig depending on
the number of messages in the queue it's consuming.

## Configuration

Example:

```yaml
.default:
  messages_per_replica: 20
  max_scale_up_step: 1
  max_scale_down_step: 1
  replicas_min: 1
  replicas_max: 5
  message_baseline: 0

app-name-1:
  deployment_config: dc-app-1
  queue: queue-app-1

app-name-2:
  deployment_config: dc-app-2
  queue: queue-app-2
  replicas_min: 2
  replicas_max: 20
```

| YAML variable          | Description                                                                |
|------------------------|----------------------------------------------------------------------------|
| `deployment_config`    | Name of the OpenShift DeploymentConfig, defaults to the app name           |
| `queue`                | Name of the RabbitMQ queue, defaults to `queue_prefix` + app name          |
| `queue_prefix`         | Prefix to derive queue names from app names                                |
| `messages_per_replica` | Number of messages a replica can handle before scaling up.                 |
| `max_scale_up_step`    | Maximum number of replicas that can be spawned on a single refresh period. |
| `max_scale_down_step`  | Maximum number of replicas that can be deleted on a single refresh period. |
| `replicas_min`         | Minumum number of replicas spawned at the same time.                       |
| `replicas_max`         | Maximum number of replicas spawned at the same time.                       |
| `message_baseline`     | Number of messages that should not be considered for scaling.              |

Ignoring the scaling steps, the target replica count is defined as

```python
max(replicas_min,
  min(replicas_max,
    round_up(
      max(0, messages - messages_baseline) / messages_per_replica
    )
  )
)
```

| Environment variable         | Description                                                                       |
|------------------------------|-----------------------------------------------------------------------------------|
| `CKI_DEPLOYMENT_ENVIRONMENT` | Define the deployment environment (production/staging)                            |
| `AUTOSCALER_CONFIG`          | Configuration in YAML. If not present, falls back to `GITLAB_CI_BOT_CONFIG_PATH`. |
| `AUTOSCALER_CONFIG_PATH`     | Path to the configuration YAML file                                               |
| `REFRESH_PERIOD`             | Time in seconds between checks (Default: 30).                                     |
| `RABBITMQ_HOST`              | RabbitMQ host                                                                     |
| `RABBITMQ_PORT`              | RabbitMQ port, TLS is used for port 443                                           |
| `RABBITMQ_USER`              | RabbitMQ user                                                                     |
| `RABBITMQ_PASSWORD`          | RabbitMQ password                                                                 |
| `RABBITMQ_CAFILE`            | RabbitMQ CA file path                                                             |
| `RABBITMQ_CERTFILE`          | RabbitMQ certificate + private key file path                                      |
| `SENTRY_SDN`                 | Sentry SDN                                                                        |
