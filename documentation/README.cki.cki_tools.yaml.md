---
title: cki.cki_tools.yaml
linkTitle: yaml
description: CLI tools that implements YAML operations missing in shyaml
---

```text
$ python3 -m cki.cki_tools.yaml --help
usage: yaml.py [-h] {set-value} key value
```

Currently, only `set-value` is implemented. The value is parsed as YAML.

Example:

```text
$ python -m cki.cki_tools.yaml set-value foo.1 'qux: true' <<< 'foo: [bar, baz]'
foo:
- bar
- qux: true
```
