---
title: cki_tools.koji_trigger
linkTitle: koji_trigger
description: Trigger CKI pipelines for kernels from Koji/Brew
aliases: [/l/koji-trigger]
---

Trigger CKI pipelines for kernels from Koji/Brew, either in response to
messages from a message bus or manually for task IDs given on the command
line.

## Usage

```shell
usage: python3 -m cki_tools.koji_trigger
                       [--gitlab-url GITLAB_URL]
                       [--koji-server-url KOJI_SERVER_URL]
                       [--koji-web-url KOJI_WEB_URL]
                       [--koji-top-url KOJI_TOP_URL]
                       [--config CONFIG] [--config-path CONFIG_PATH]
                       [--task-id TASK_ID]
                       [--variables KEY=VALUE]

options:
  -h, --help            show this help message and exit
  --gitlab-url GITLAB_URL
                        GitLab URL
  --koji-server-url KOJI_SERVER_URL
                        URL of XMLRPC server
  --koji-web-url KOJI_WEB_URL
                        URL of the Koji web interface
  --koji-top-url KOJI_TOP_URL
                        URL for Koji file access
  --config CONFIG       YAML configuration file to use
  --config-path CONFIG_PATH
                        Path to YAML configuration file
  --task-id TASK_ID     Koji/Brew task ID
  --variables KEY=VALUE
                        Override trigger variables
```

## Running as a microservice

```bash
python -m cki_tools.koji_trigger
```

### Configuration via environment variables

The following variables need to be defined:

| Name                         | Type   | Secret | Required | Description                                                                                                |
|------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `RABBITMQ_HOST`              | string | no     | no       | AMQP host                                                                                                  |
| `RABBITMQ_PORT`              | int    | no     | no       | AMQP port, TLS is used for port 443                                                                        |
| `RABBITMQ_VIRTUAL_HOST`      | string | no     | no       | AMQP vhost, defaults to /                                                                                  |
| `RABBITMQ_USER`              | string | no     | no       | AMQP user                                                                                                  |
| `RABBITMQ_PASSWORD`          | string | yes    | no       | AMQP password                                                                                              |
| `RABBITMQ_CAFILE`            | path   | no     | no       | AMQP CA file path                                                                                          |
| `WEBHOOK_RECEIVER_EXCHANGE`  | string | no     | no       | AMQP exchange name                                                                                         |
| `KOJI_TRIGGER_QUEUE`         | string | no     | yes      | AMQP queue name to use on the exchange                                                                     |
| `KOJI_TRIGGER_ROUTING_KEYS`  | list   | no     | yes      | AMQP routing keys, space-separated                                                                         |
| `KOJI_TRIGGER_CONFIG`        | yaml   | no     | no       | Configuration in YAML. If not present, falls back to `KOJI_TRIGGER_CONFIG_PATH`                            |
| `KOJI_TRIGGER_CONFIG_PATH`   | path   | no     | no       | Path to the configuration YAML file                                                                        |
| `KOJI_SERVER_URL`            | url    | no     | yes      | URL of XMLRPC server                                                                                       |
| `KOJI_WEB_URL`               | url    | no     | yes      | URL of the Koji web interface                                                                              |
| `KOJI_TOP_URL`               | url    | no     | yes      | URL for Koji file access                                                                                   |
| `PIPELINE_DEFINITION_URL`    | url    | no     | yes      | URL of the pipeline-definition repository                                                                  |
| `GITLAB_PARENT_PROJECT`      | string | no     | yes      | Parent group of pipeline projects                                                                          |
| `GITLAB_URL`                 | url    | no     | yes      | URL of GitLab instance                                                                                     |
| `GITLAB_TOKENS`              | json   | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`               | string | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                                                     |
| `CKI_DEPLOYMENT_ENVIRONMENT` | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`          | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `CKI_METRICS_ENABLED`        | bool   | no     | no       | Enable prometheus metrics. Default: false                                                                  |
| `CKI_METRICS_PORT`           | int    | no     | no       | Port where prometheus metrics are exposed. Default: 8000                                                   |
| `SENTRY_DSN`                 | url    | yes    | no       | Sentry DSN                                                                                                 |

### Prometheus metrics

If `CKI_METRICS_ENABLED` is `true`, prometheus metrics are exposed on the `CKI_METRICS_PORT` port.

The exposed data is the following:

| Name                                | Type      | Description                                               |
|-------------------------------------|-----------|-----------------------------------------------------------|
| `cki_koji_pipeline_triggered`       | Counter   | Number of Koji/Brew pipelines triggered                   |
| `cki_http_request_duration_seconds` | Histogram | Time spent making a request                               |
| `cki_http_requests_by_status`       | Counter   | Number of requests by status                              |
| `cki_message_received`              | Counter   | Number of queue messages received                         |
| `cki_message_processed`             | Counter   | Number of queue messages processed                        |
| `cki_message_error`                 | Counter   | Number of exceptions during queue message handling        |
| `cki_message_load`                  | Summary   | Normalized indicator of the time spent handling a message |
| `cki_message_duration_seconds`      | Histogram | Time spent handling a message                             |

## Triggering via the command line

```bash
python3 -m cki.cki_tools.koji_trigger \
    --gitlab-url GITLAB_URL \
    --config-path CONFIG_PATH \
    --task-id TASK_ID \
    --server-section SERVER_SECTION \
    --variables KEY=VALUE ...
```

### Additional configuration via environment variables

The following variables need to be defined:

| Name                         | Type   | Secret | Required | Description                                                                                                |
|------------------------------|--------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `PIPELINE_DEFINITION_URL`    | url    | no     | yes      | URL of the pipeline-definition repository                                                                  |
| `GITLAB_PARENT_PROJECT`      | string | no     | yes      | Parent group of pipeline projects                                                                          |
| `GITLAB_TOKENS`              | json   | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`               | string | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS`                                                     |
| `CKI_DEPLOYMENT_ENVIRONMENT` | string | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`          | string | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `SENTRY_DSN`                 | url    | yes    | no       | Sentry DSN                                                                                                 |

## Pipeline configuration YAML

Basic inheritance and defaults are modeled after the
[default](https://docs.gitlab.com/ce/ci/yaml/#global-defaults) and
[extends](https://docs.gitlab.com/ce/ci/yaml/#extends) keywords in
the [GitLab CI/CD job descriptions](https://docs.gitlab.com/ce/ci/yaml) can be
used.

* trigger configurations with a name starting with a dot (`.`) are not
  processed, but can be used for inheritance
* the `.default` trigger configuration will be inherited by all trigger
  configurations
* the configuration option `.extends` followed by a name or list of names
  specifies trigger configuration(s) to inherit from
* configuration options with a name starting with a dot (`.`) are used to
  configure the trigger, but are NOT passed to the pipeline code as trigger
  variables

### Supported configuration options

Next to variables that are passed through to the pipeline as trigger variables,
certain configuration options are required by the Koji/Brew trigger.

| Name                   | Description                                                         |
|------------------------|---------------------------------------------------------------------|
| `source_package_name`  | regex to select the correct source packages                         |
| `rpm_release`          | regex to select the correct RPM releases                            |
| `.test_scratch`        | also test scratch builds and builds without tags, defaults to false |
| `.report_rules`        | serialized via JSON to `report_rules` trigger variable              |
| `cki_pipeline_project` | GitLab pipeline project                                             |
| `cki_pipeline_branch`  | target branch in the GitLab pipeline project                        |
