---
title: cki.cki_tools.service_metrics
linkTitle: service_metrics
description: Service metrics exposed via Prometheus
---

The service metrics framework runs collector modules on specific schedules to
generate various metrics about CKI or external services.

## Qualys

`cki.cki_tools.service_metrics.metrics.qualys`

This collector

- retrieves Qualys vulnerability reports in CSV format from a POP3 email inbox
- uploads the raw reports to an S3 bucket
- uploads a clean version of the last report to an S3 bucket which can be used
  in Grafana dashboards
- exposes aggregate metrics about vulnerabilities

### Configuration

| Environment variable | Type   | Secret | Description                                                                     |
|----------------------|--------|--------|---------------------------------------------------------------------------------|
| `QUALYS_CONFIG`      | YAML   | no     | Configuration for the collector                                                 |
| `BUCKET_CONFIG`      | string | yes    | S3 bucket specification for the reports as configured in `QUALYS_CONFIG.config` |
| `POP3_PASSWORD`      | string | yes    | password for the POP3 email inbox as configured in `QUALYS_CONFIG.pop3.pass`    |

The configuration in `QUALYS_CONFIG` can be provided in YAML or JSON format:

```yaml
---
bucket: BUCKET_CONFIG                  # name of the environment variable that contains the bucket specification
pop3:                                  # email inbox configuration
  host: pop.googlemail.com             # host name for POP3 access
  port: 995                            # port for POP3 over TLS/SSL
  user: cki-qualys-reports@redhat.com  # user name for the POP3 inbox
  pass: POP3_PASSWORD                  # name of the environment variable that contains the password/token for the POP3 inbox
stale_threshold: 2                     # number of days to consider a report up-to-date based on the email date (default: 2)
grace_threshold: 7                     # number of days to wait for vulnerabilites to be fixed before alerting (default: 7)
```

### Metrics

- `cki_qualys_reports{host,type}`: gauge that represents the number of
  vulnerabilities reported by Qualys:
  - `host`: host DNS name as registered with Qualys
  - `type`: type of reported vulnerability: already fixed on the hosts
    (`fixed`), active but still within the grace period (`grace`), active older
    than the grace period (`active`), and total number of reported
    vulnerabilities (`total`)
