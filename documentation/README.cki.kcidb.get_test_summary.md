---
title: cki.kcidb.get_test_summary
linkTitle: get_test_summary
description: Summarizes status from tests within a KCIDB file
---

```bash
usage: get_test_summary.py [-h] kcidb_file

Get test execution summary from a KCIDB file.

positional arguments:
  kcidb_file  A path to the KCIDB file to read.

optional arguments:
  -h, --help  show this help message and exit
```

**get_test_summary** is a CLI tool to parse an artifact called `kcidb_all.json`,
which follows the [KCIDB schema][kcidb], and contains, among other things,
the results of tests from which the most relevant will be returned following the
priority order (highest to lowest): "FAIL", "ERROR", "MISS", and "PASS".

{{% alert title="NOTE" color="info" %}}

The status "SKIP" and "DONE" are parsed as "PASS"

{{% /alert %}}

[kcidb]: https://github.com/kernelci/kcidb-io

---

This tool is used at [pipeline-definition], specifically on pipelines that do not
include the `check-kernel-results` job, which relies on the [reporter].

[pipeline-definition]: https://gitlab.com/cki-project/pipeline-definition/-/blob/main/pipeline/stages/test.yml
[reporter]: https://gitlab.com/cki-project/reporter
