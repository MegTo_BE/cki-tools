---
title: cki_tools.umb_messenger
linkTitle: umb_messenger
description: Webhook responsible for UMB message sending to trigger external testing
---

[External testing] is triggered via UMB messages.

The messenger can be deployed locally by running

```bash
python -m cki_tools.umb_messenger
```

Note that you need the appropriate environment variables exposed and have UMB
certificate and configuration in the right place!

Required environment variables:

## Configuration via environment variables

| Name                                | Secret | Required | Description                                                                                                |
|-------------------------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `DATAWAREHOUSE_URL`                 | no     | yes      | DataWarehouse URL                                                                                          |
| `DATAWAREHOUSE_TOKEN_UMB_MESSENGER` | yes    | yes      | DataWarehouse access token                                                                                 |
| `STOMP_HOST`                        | no     | yes      | UMB host                                                                                                   |
| `STOMP_PORT`                        | no     | yes      | UMB port                                                                                                   |
| `STOMP_CERTFILE`                    | yes    | yes      | UMB certificate                                                                                            |
| `RABBITMQ_HOST`                     | no     | no       | AMQP host                                                                                                  |
| `RABBITMQ_PORT`                     | no     | no       | AMQP port, TLS is used for port 443                                                                        |
| `RABBITMQ_VIRTUAL_HOST`             | no     | no       | AMQP vhost, defaults to /                                                                                  |
| `RABBITMQ_USER`                     | no     | no       | AMQP user                                                                                                  |
| `RABBITMQ_PASSWORD`                 | yes    | no       | AMQP password                                                                                              |
| `RABBITMQ_CAFILE`                   | no     | no       | AMQP CA file path                                                                                          |
| `WEBHOOK_RECEIVER_EXCHANGE`         | no     | no       | AMQP exchange to receive messages                                                                          |
| `UMB_MESSENGER_QUEUE`               | no     | no       | AMQP queue name to use on the exchange                                                                     |
| `UMB_MESSENGER_ROUTING_KEYS`        | no     | no       | AMQP routing keys for the messages sent to the queue                                                       |
| `CKI_DEPLOYMENT_ENVIRONMENT`        | no     | no       | Define the deployment environment (production/staging)                                                     |
| `CKI_LOGGING_LEVEL`                 | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |
| `CKI_METRICS_ENABLED`               | no     | no       | Enable prometheus metrics. Default: false                                                                  |
| `CKI_METRICS_PORT`                  | no     | no       | Port where prometheus metrics are exposed. Default: 8000                                                   |

## Manual triggering

Messaging can also be manually triggered via the command line. First, make
sure to have the required environment variables above configured correctly.
Then, the sending of UMB messages can be triggered with

```bash
CKI_LOGGING_LEVEL=INFO \
  CKI_DEPLOYMENT_ENVIRONMENT=production \
  python3 -m cki_tools.umb_messenger \
  --message-type MESSAGE_TYPE \
  --checkout-id CHECKOUT_ID
```

* `MESSAGE_TYPE` can be `pre_test` or `post_test`.
* `CHECKOUT_ID` is the KCIDB checkout ID.

[External testing]: https://cki-project.org/l/testing-cki-builds
