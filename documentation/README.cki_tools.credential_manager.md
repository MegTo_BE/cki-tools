---
title: cki_tools.credential_manager
linkTitle: credential_manager
description: Manage CKI service account secrets across services
aliases: [/l/credential-manager-docs]
---

Manage credentials for service accounts and their meta data as stored in CKI secrets.

## Life cycle

In general, expiring tokens have multiple versions that go through the
following life-cycle:

| description                                        | `active` | `deployed` |
|----------------------------------------------------|----------|------------|
| created and deployed                               | `true`   | `true`     |
| superseded and no longer deployed, but still valid | `true`   | `false`    |
| revoked and no longer valid                        | `false`  | `false`    |

## Metrics

Usage:

```bash
python -m cki_tools.credential_manager metrics
```

This will output the following Prometheus metrics related to the stored credentials:

| name                   | supported `token_type`           | description         |
|------------------------|----------------------------------|---------------------|
| `cki_token_expires_at` | `bugzilla_token`, `gitlab_token` | ISO8601 expiry date |

## GitLab

For various kinds of GitLab tokens, the tool supports

- creation (`create`)

  ```bash
  python -m cki_tools.credential_manager gitlab create --token TOKEN_SECRET_NAME
  ```

- rotation (`rotate`)

  ```bash
  python -m cki_tools.credential_manager gitlab rotate
                          [--token TOKEN] [--dry-run] [--force]

  options:
    --token TOKEN         Only rotate a single token
    --dry-run             Do not modify secrets or create tokens
    --force               Force token rotation even if new enough
  ```

- meta data update (`update`)

  ```bash
  python -m cki_tools.credential_manager gitlab update
  ```

- validation of deployed tokens (`validate`)

  ```bash
  python -m cki_tools.credential_manager gitlab validate
  ```

### Project access tokens

See the [API description][project-api] for details.

| name                | create   | rotate   | update   | validate | description                          |
|---------------------|----------|----------|----------|----------|--------------------------------------|
| (secret)            |          |          |          | required | secret token                         |
| `token_type`        | required | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | required | `project_token`                      |
| `project_url`       | required | required | required | required | Project URL                          |
| `scopes`            | required | required | updated  |          | Access scope                         |
| `access_level`      | required | required | updated  |          | Access levels                        |
| `token_name`        | required | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | updated  | required |          | Project access token ID              |
| `created_at`        | updated  | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  | updated  |          | Whether the token is still active    |
| `user_id`           | updated  | updated  | updated  |          | ID of associated user                |
| `user_name`         | updated  | updated  | updated  |          | Name of associated user              |
| `deployed`          | updated  | updated  |          |          | Whether the token is actually used   |

### Group access tokens

See the [API description][group-api] for details.

| name                | create   | rotate   | update   | validate | description                          |
|---------------------|----------|----------|----------|----------|--------------------------------------|
| (secret)            |          |          |          | required | secret token                         |
| `token_type`        | required | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | required | `group_token`                        |
| `group_url`         | required | required | required | required | Group URL                            |
| `scopes`            | required | required | updated  |          | Access scope                         |
| `access_level`      | required | required | updated  |          | Access levels                        |
| `token_name`        | required | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | updated  | required |          | Group access token ID                |
| `created_at`        | updated  | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  | updated  |          | Whether the token is still active    |
| `user_id`           | updated  | updated  | updated  |          | ID of associated user                |
| `user_name`         | updated  | updated  | updated  |          | Name of associated user              |
| `deployed`          | updated  | updated  |          |          | Whether the token is actually used   |

### Personal access tokens

See the [API description][personal-api] for details.

Token creation is not supported.

| name                | rotate   | update   | validate | description                          |
|---------------------|----------|----------|----------|--------------------------------------|
| (secret)            |          |          | required | secret token                         |
| `token_type`        | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | `personal_token`                     |
| `instance_url`      | required | required | required | GitLab instance URL                  |
| `scopes`            | updated  | updated  |          | Access scope                         |
| `token_name`        | updated  | updated  |          | Name of the token                    |
| `token_id`          | required | required |          | Access token ID                      |
| `created_at`        | updated  | updated  |          | ISO8601 timestamp of creation        |
| `expires_at`        | updated  | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  |          | Whether the token is still active    |
| `user_id`           | required | required |          | ID of associated user                |
| `user_name`         | updated  | updated  |          | Name of associated user              |
| `deployed`          | updated  |          |          | Whether the token is actually used   |

### Project deploy tokens

See the [API description][deploy-api] for details.

Token rotation is not supported.

| name                | create   | update   | validate | description                          |
|---------------------|----------|----------|----------|--------------------------------------|
| (secret)            |          |          |          | secret token                         |
| `token_type`        | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | `project_deploy_token`               |
| `project_url`       | required | required | required | Project URL                          |
| `scopes`            | required | updated  |          | Access scope                         |
| `token_name`        | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | required | required | Project deploy token ID              |
| `created_at`        | updated  |          |          | ISO8601 timestamp of creation        |
| `expires_at`        | optional | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  |          | Whether the token is still active    |
| `user_name`         | updated  | updated  |          | Associated user name                 |
| `deployed`          | updated  |          |          | Whether the token is actually used   |

### Group deploy tokens

See the [API description][deploy-api] for details.

Token rotation is not supported.

| name                | create   | update   | validate | description                          |
|---------------------|----------|----------|----------|--------------------------------------|
| (secret)            |          |          |          | secret token                         |
| `token_type`        | required | required | required | `gitlab_token`                       |
| `gitlab_token_type` | required | required | required | `group_deploy_token`                 |
| `group_url`         | required | required | required | Group URL                            |
| `scopes`            | required | updated  |          | Access scope                         |
| `token_name`        | required | updated  |          | Name of the token                    |
| `token_id`          | updated  | required | required | Group deploy token ID                |
| `created_at`        | updated  |          |          | ISO8601 timestamp of creation        |
| `expires_at`        | optional | updated  |          | ISO8601 expiry date                  |
| `revoked`           | updated  | updated  |          | Whether the token is already revoked |
| `active`            | updated  | updated  |          | Whether the token is still active    |
| `user_name`         | updated  | updated  |          | Associated user name                 |
| `deployed`          | updated  |          |          | Whether the token is actually used   |

### Runner authentication tokens

See the [API description][runner-api] for details.

Token creation and rotation is not supported.

| name                | update   | validate | description                        |
|---------------------|----------|----------|------------------------------------|
| (secret)            | required | required | secret token                       |
| `token_type`        | required | required | `gitlab_token`                     |
| `gitlab_token_type` | required | required | `runner_authentication_token`      |
| `instance_url`      | required | required | GitLab instance URL                |
| `token_id`          | updated  |          | Group token ID                     |
| `expires_at`        | updated  |          | ISO8601 expiry date (optional)     |
| `active`            | updated  |          | Whether the token is still active  |
| `deployed`          |          |          | Whether the token is actually used |

## Configuration via environment variables

| Name                | Secret | Required | Description                                                                                                |
|---------------------|--------|----------|------------------------------------------------------------------------------------------------------------|
| `GITLAB_TOKENS`     | no     | yes      | URL/environment variable pairs of GitLab instances and private tokens                                      |
| `GITLAB_TOKEN`      | yes    | yes      | GitLab private tokens as configured in `GITLAB_TOKENS` above                                               |
| `CKI_LOGGING_LEVEL` | no     | no       | logging level for CKI modules, defaults to WARN; to get meaningful output on the command line, set to INFO |

[project-api]: https://docs.gitlab.com/ee/api/project_access_tokens.html
[group-api]: https://docs.gitlab.com/ee/api/group_access_tokens.html
[personal-api]: https://docs.gitlab.com/ee/api/personal_access_tokens.html
[deploy-api]: https://docs.gitlab.com/ee/api/deploy_tokens.html
[runner-api]: https://docs.gitlab.com/ee/api/runners.html
