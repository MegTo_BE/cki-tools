---
title: cki.deployment_tools.secrets
linkTitle: secrets
description: Access CKI secrets
aliases: [/l/secrets-helper-docs]
---

The `secrets` tool allows access to CKI variables and secrets.

```text
$ python3 -m cki.deployment_tools.secrets --help
usage: secrets.py [-h] {secret,variable,edit,validate,login,logout} ...

Access CKI variables and secrets

positional arguments:
  {secret,variable,edit,validate,login,logout}
    secret              Retrieve a secret value
    variable            Retrieve a variable value
    edit                Edit a secret value
    validate            Validate stored secrets
    login               Log into secrets storage
    logout              Log out of secrets storage

  -h, --help            show this help message and exit
  --json                output in json format
```

The following CLI aliases are provided:

- `cki_secret` == `python3 -m cki.deployment_tools.secrets secret`
- `cki_variable` == `python3 -m cki.deployment_tools.secrets variable`
- `cki_edit_secret` == `python3 -m cki.deployment_tools.secrets edit`
- `cki_secrets_validate` == `python3 -m cki.deployment_tools.secrets validate`
- `cki_secrets_login` == `python3 -m cki.deployment_tools.secrets login`
- `cki_secrets_logout` == `python3 -m cki.deployment_tools.secrets logout`

CKI variables and secrets meta data are stored in YAML files. Encrypted secrets
are stored in HashiCorp Vault. A legacy YAML backend that stores encrypted
secrets inline next to the secrets meta data is provided until all secrets are
migrated to HashiCorp Vault.

## Environment variables

| Name                      | Secret | Required | Description                                                    |
|---------------------------|--------|----------|----------------------------------------------------------------|
| `CKI_SECRETS_FILE`        | no     | yes      | Path to the secrets meta data file                             |
| `CKI_VARS_FILE`           | no     | yes      | Path to the variables file                                     |
| `CKI_VARS_NAMES`          | no     | no       | Additional variable names with variable file names             |
| `VAULT_ADDR`              | no     | yes      | Address of the Vault server expressed as a URL, including port |
| `VAULT_MOUNT_POINT`       | no     | no       | Mount point of the KV2 secrets engine                          |
| `VAULT_APPROLE_SECRET_ID` | yes    | no       | Secret ID of the approle (service account)                     |
| `VAULT_TOKEN`             | yes    | no       | Vault client token, falls back to `~/.vault-token` if not set  |

## `cki_secret`

```text
usage: secrets.py secret [-h] [--json] key

Retrieve a secret value

positional arguments:
  key         secret name

options:
  -h, --help  show this help message and exit
  --json      output in json format
```

For secrets, the file name for the secrets meta data needs to be specified in
the `CKI_SECRETS_FILE` environment variable. The actual secrets are stored in
HashiCorp Vault.

As an example, a secrets meta data file could look like:

```yaml
foo:
  meta:
    token_type: aws_secret_access_key
```

Pointing `CKI_SECRETS_FILE` to it and calling `cki_secret foo` will try to
retrieve the value of `foo` from HashiCorp Vault:

```shell
$ cki_secret foo
bar
$ cki_secret foo --json
"bar"
```

### Conditions

Conditions allow to obtain a list of meta/data fields for multiple secrets via
something like `path[meta-key-1,!meta-key-2,...]`. For all secrets that start
with the given path, delimited by `/`, the given meta fields are obtained with
a default of `False`. A secret is only included in the returned list if all
conditions match, i.e. if they are `True` for a condition like `meta-key-1`, or
`False` for a condition like `!meta-key-2`.

### Supported locations

| Location                 | Description                                |
|--------------------------|--------------------------------------------|
| `some/path#`             | dictionary of all key-value meta pairs     |
| `some/path#field`        | value for the given meta field             |
| `some/path:`             | dictionary of all key-value data pairs     |
| `some/path:field`        | value for the given data field             |
| `some/path`              | value for the `value` data field           |
| `some/path[cond1,cond2]` | list of the above for all matching secrets |

## `cki_variable`

```text
usage: secrets.py variable [-h] [--json] key

Retrieve a variable value

positional arguments:
  key         variable name

options:
  -h, --help  show this help message and exit
  --json      output in json format
```

For variables, the file name needs to be specified in the `CKI_VARS_FILE`
environment variable. A list of space-delimited additional environment variable
names can be provided in the `CKI_VARS_NAMES` environment variable.

As an example, a variables file could look like:

```yaml
foo: bar
baz: |
  some
  string
qux:
  complex: value
bool: true
int: 15
```

Pointing `CKI_VARS_FILE` to it and calling `cki_variable key` will print the
values of the various variables:

```shell
$ for i in foo baz qux bool int; do cki_variable $i; done
bar
some
string
{'complex': 'value'}
True
15
```

With `--json`, the output will be properly json-encoded:

```shell
$ for i in foo baz qux bool int; do cki_variable --json $i; done
"bar"
"some\nstring"
{"complex": "value"}
true
15
```

## `cki_edit_secret`

```text
usage: secrets.py edit [-h] key value

Edit a secret value

positional arguments:
  key         secret name
  value       new secret value

options:
  -h, --help  show this help message and exit
```

Secrets and their associated meta data can be edited as well. From the
location syntax table above, all but the conditions are supported.

When editing meta data, the secrets meta data file is rewritten with the
updated meta data. When editing secrets, the actual secret values are updated
on HashiCorp Vault.

```shell
$ cat secrets.yml
foo:
  meta:
    token_type: aws_secret_access_key
$ cki_edit_secret foo#user_name aws-user-name
$ cat secrets.yml
foo:
  meta:
    token_type: aws_secret_access_key
    user_name: aws-user-name
```

## `cki_secrets_validate`

```text
usage: secrets.py validate [-h]

Validate stored secrets

options:
  -h, --help  show this help message and exit
```

Compare the list of secrets available in HashiCorp Vault and the secrets meta data.
Exits with an exit code of 2 if secrets are missing in HashiCorp Vault.
Exits with an exit code of 1 if secrets are only missing in the secrets meta data.
Exits with an exit code of 0 if no difference is found.

## `cki_secrets_login`

```text
usage: secrets.py login [-h] [--duration DURATION] (--oidc | --approle APPROLE)

Log into secrets storage

options:
  -h, --help           show this help message and exit
  --duration DURATION  validity of the token
  --oidc               login via OIDC
  --approle APPROLE    login via given approle
```

Log into HashiCorp Vault either via OIDC (human user) or an approle (service account).

For OIDC, a Kerberos ticket is necessary to get past SSO.

For an approle, the approle secret ID needs to be provided in the
`VAULT_APPROLE_SECRET_ID` environment variable.

If successful, the client token will be stored in `~/.vault-token` as supported
by the official `vault` CLI tooling.

## `cki_secrets_logout`

```text
usage: secrets.py logout [-h]

Log out of secrets storage

options:
  -h, --help  show this help message and exit
```

Log out of HashiCorp Vault by revoking the client token. Independent whether
the API call is successful, the `~/.vault-token` file will be removed.
