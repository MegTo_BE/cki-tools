---
title: cki_deployment_git_s3_sync.sh
description: Sync a git repository to an S3 bucket
---

```shell
cki_deployment_git_s3_sync.sh
```

## Environment variables

| Name                 | Secret | Required | Description                                                   |
|----------------------|--------|----------|---------------------------------------------------------------|
| `BUCKET_CONFIG_NAME` | no     | yes      | name of environment variable with the bucket specification    |
| `BUCKET_CONFIG`      | yes    | yes      | bucket specification as configured in `BUCKET_CONFIG_NAME`    |
| `REPO_URL`           | no     | yes      | git repository to sync                                        |
| `REPO_REF`           | no     | yes      | git branch/tag to sync                                        |
| `REPO_SUBDIR`        | no     | no       | subdirectory of the git repository to sync, defaults to empty |
