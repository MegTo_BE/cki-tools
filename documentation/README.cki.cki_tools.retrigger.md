---
title: cki.cki_tools.retrigger
linkTitle: retrigger
description: Trigger canary pipelines based on production pipelines
---

Tool to retrigger production pipelines, while optionally modifying their configuration.

## Usage

```shell
Usage: python3 -m cki.cki_tools.retrigger \
    [--variables KEY=VALUE]... pipeline_url
```

If `CKI_DEPLOYMENT_ENVIRONMENT` is set to production, the trigger variables of
the production pipelines are not modified. Otherwise, pipelines are marked as
retriggered which causes them to be ignored by all production machinery.

## Configuration

| Environment variable         | Required | Type   | Secret | Description                                                           |
|------------------------------|----------|--------|--------|-----------------------------------------------------------------------|
| `GITLAB_TOKENS`              | yes      | yes    | no     | URL/environment variable pairs of GitLab instances and private tokens |
| `GITLAB_TOKEN`               | yes      | yes    | yes    | GitLab private tokens as configured in `GITLAB_TOKENS` above          |
| `CKI_DEPLOYMENT_ENVIRONMENT` | no       | string | no     | Mark pipelines as retriggered if not `production`                     |
