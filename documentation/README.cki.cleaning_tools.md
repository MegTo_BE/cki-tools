---
title: cki.cki_tools.cleaning_tools
linkTitle: cleaning_tools
description: Check for unused artifacts in the AWS storage
---

__Check for unused artifacts in the AWS storage__
There are two files.
The file `get_gitlab_data.py` is for getting and operation information from Gitlab.
The file `s3bucketobjects.py` is for getting and operation information from AWS buckets.

__Configure access to AWS accounts__
Use the internal companion page which contain additional information.

__Export environment variables:__

```shell
export AWS_PROFILE="aws profile here"
export CKI_LOGGING_LEVEL=INFO

export S3_CONFIG='buckets:
- name: "my AWS name"
  bucket: "my AWS bucket"
  endpoint: "my endpoint https://..."
  access_key: "my access key"
  secret_key: "my secret key"
  prefix: "my AWS bucket prefix"
  data: "additional data goes here"'
```

__Usage:__

```shell
usage: cleaning_tools.py [-h] -a CREATED_AFTER -b CREATED_BEFORE -u GITLAB_URL

options:
  -h, --help            show this help message and exit

Required named arguments:
  -a CREATED_AFTER, --created_after CREATED_AFTER
                        Returns merge requests created on or after the given time. Expected in ISO 8601 format (2022-10-29T08:00:00Z).
  -b CREATED_BEFORE, --created_before CREATED_BEFORE
                        Returns merge requests created on or before the given time. Expected in ISO 8601 format (2022-11-28T08:00:00Z).
  -u GITLAB_URL, --gitlab_url GITLAB_URL
                        GitLab URL. Expected GitLab Project: https://gitlab.com/redhat/rhel/src/kernel/rhel-9 https://gitlab.com/redhat/centos-
                        stream/src/kernel/centos-stream-9
```

__Usage example:__

```shell
export CKI_LOGGING_LEVEL=INFO
python cleaning_tools.py -a 2022-12-20T08:00:00Z -b 2023-02-07T08:00:00Z -u https://gitlab.com/redhat/centos-stream/src/kernel/centos-stream-9
```
