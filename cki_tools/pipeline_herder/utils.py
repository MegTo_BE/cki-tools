"""Various utils used across the pipeline herder."""
import dataclasses
import enum
from functools import cached_property
from functools import lru_cache
import json
import pathlib
import re
from tempfile import NamedTemporaryFile
import typing

from cki_lib import chatbot
from cki_lib import gitlab
from cki_lib import misc
from cki_lib.kcidb.file import KCIDBFile
from cki_lib.logger import get_logger
from cki_lib.session import get_session

from . import settings

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)


class RetryReason(enum.Enum):
    """Reason why a retry should (not) be performed."""

    def __init__(self, message: str, notify_chat: bool = False, retry: bool = False) -> None:
        """Create a new reason."""
        self.message = message
        self.notify_chat = notify_chat
        self.retry = retry

    RETRY_OK = ('everything fine', True, True)
    RETRY_LIMIT_REACHED = ('retry limit reached', True)
    RETRIGGERED = 'retriggered job'
    EXTERNALLY_RESTARTED = 'job restarted externally'
    HERDER_ACTION = 'not configured for retries'


@dataclasses.dataclass
class RetryRating:
    """Rating whether a retry should be performed."""

    reason: RetryReason
    details: typing.Optional[str] = None

    @property
    def message(self) -> str:
        """Return the combined message including optional details."""
        if self.details:
            return f'{self.reason.message} ({self.details})'
        return self.reason.message


class CachedJob:
    """Cache information about a GitLab job to minimize API calls."""

    def __init__(self, web_url):
        """Initialize a cached job."""
        self.gl_instance, self.gl_job = gitlab.parse_gitlab_url(web_url)
        # https://rednafi.github.io/reflections/dont-wrap-instance-methods-with-functoolslru_cache-decorator-in-python.html
        self.artifact_file = lru_cache()(self._artifact_file)

    @cached_property
    def gl_project(self):
        """Return the GitLab project."""
        return self.gl_instance.projects.get(self.gl_job.pipeline['project_id'])

    @cached_property
    def gl_pipeline(self):
        """Return the GitLab pipeline."""
        return self.gl_project.pipelines.get(self.gl_job.pipeline['id'])

    @cached_property
    def gl_pipeline_jobs(self):
        """Return all jobs of the GitLab pipeline."""
        return self.gl_pipeline.jobs.list(all=True, include_retried=True)

    @cached_property
    def variables(self):
        """Return the GitLab pipeline variables."""
        return {v.key: v.value for v in self.gl_pipeline.variables.list()}

    @cached_property
    def retriggered(self):
        """Return whether the pipeline was retriggered."""
        return self.variables.get('CKI_DEPLOYMENT_ENVIRONMENT', 'production') != 'production'

    @cached_property
    def trace(self):
        """Return the GitLab job trace."""
        return self.gl_job.trace().decode('utf8', errors='replace').split('\n')

    @cached_property
    def artifacts_meta(self) -> dict:
        """Return the artifact meta data."""
        try:
            return json.loads(self.gl_job.artifact('artifacts-meta.json'))
        # pylint: disable=broad-except
        except Exception:
            return {'mode': 'gitlab'}

    def _s3_artifact_file(self, name):
        """Return one artifact file from S3."""
        response = SESSION.head(f'{self.artifacts_meta["s3_url"]}/{name}')
        response.raise_for_status()
        # prevent the herder from becoming a victim of the OOM killer
        if ((length := int(response.headers.get('content-Length', '0')))
                > settings.HERDER_MAXIMUM_ARTIFACT_SIZE):
            raise ValueError(f'Artifact file {name} too big: {length}')
        response = SESSION.get(f'{self.artifacts_meta["s3_url"]}/{name}')
        response.raise_for_status()
        return response.content

    def _gitlab_artifact_file(self, name):
        """Return one artifact file from GitLab."""
        return self.gl_job.artifact(name)

    def _artifact_file(self, name, *, split=True):
        """Return one artifact file or None/an empty list if not found."""
        try:
            if self.artifacts_meta['mode'] == 'gitlab':
                artifact = self._gitlab_artifact_file(name)
            else:
                artifact = self._s3_artifact_file(name)
        # pylint: disable=broad-except
        except Exception:
            return [] if split else None
        artifact = artifact.decode('utf-8', errors='replace')
        return artifact.split('\n') if split else artifact

    @cached_property
    def auth_user_id(self):
        """Return the user owning the GitLab connection."""
        instance = self.gl_instance
        if not hasattr(instance, 'user'):
            instance.auth()
        return instance.user.id

    def job_name_count(self):
        """Return the number of jobs in the pipeline with the same name."""
        return len([j for j in self.gl_pipeline_jobs
                    if j.name == self.gl_job.name])

    def retry_delay(self):
        """Return the number of minutes to wait before a retry."""
        delay_index = self.job_name_count() - 1
        if delay_index >= len(settings.HERDER_RETRY_DELAYS):
            return settings.HERDER_RETRY_DELAYS[-1]
        return settings.HERDER_RETRY_DELAYS[delay_index]

    def retry_rating(self) -> RetryRating:
        # pylint: disable=too-many-return-statements
        """Check whether a job can be safely retried by the herder."""
        # do not retry retriggered pipelines
        if self.retriggered:
            return RetryRating(RetryReason.RETRIGGERED)

        # there should be no newer job with the same name
        newer_jobs = [f'J{j.id}' for j in self.gl_pipeline_jobs
                      if j.name == self.gl_job.name and j.id > self.gl_job.id]
        if newer_jobs:
            return RetryRating(RetryReason.EXTERNALLY_RESTARTED,
                               details=', '.join(newer_jobs))

        # only retry up to a maximum number of times
        if self.job_name_count() > settings.HERDER_RETRY_LIMIT:
            return RetryRating(RetryReason.RETRY_LIMIT_REACHED,
                               details=str(settings.HERDER_RETRY_LIMIT))

        # only retry if the herder is configured to do so
        if settings.HERDER_ACTION != 'retry':
            return RetryRating(RetryReason.HERDER_ACTION,
                               details=settings.HERDER_ACTION)

        return RetryRating(RetryReason.RETRY_OK)


class Matcher:  # pylint: disable=too-few-public-methods
    """Base class for matchers."""

    # pylint: disable=too-many-arguments,too-many-instance-attributes
    def __init__(self, name, description, messages,
                 job_name=None, job_status=('failed',), file_name=None,
                 action='retry', tail_lines=300):
        """Initialize the matcher."""
        self.name = name
        self.description = description
        self.job_name = job_name
        self.job_status = job_status
        self.file_name = file_name
        self.action = action
        self.messages = list(misc.flattened(messages))
        self.tail_lines = tail_lines

    @staticmethod
    def _check_lines(message, lines):
        """Check lines for a match."""
        if isinstance(message, re.Pattern):
            return message.search('\n'.join(lines))
        return any(message in line for line in lines)

    def check_lines(self, lines):
        """Check lines for a match."""
        return any(self._check_lines(m, lines) for m in self.messages)

    def check(self, job: CachedJob):
        """Check status/log for a match."""
        if job.gl_job.status not in self.job_status:
            return False
        if self.job_name and not job.gl_job.name.startswith(self.job_name):
            return False
        if self.file_name:
            all_lines = job.artifact_file(self.file_name, split=True)
        else:
            all_lines = job.trace

        return self.check_lines(all_lines[-self.tail_lines:])


class TimeoutMatcher:  # pylint: disable=too-few-public-methods
    """Check whether a job got stuck or hit a timeout."""

    name = 'timeout'
    description = 'Job got stuck or hit timeout'
    action = 'retry'

    def check(self, job: CachedJob):  # pylint: disable=no-self-use
        """Check status/failure reason for a match."""
        if job.gl_job.status != 'failed':
            return False
        if job.gl_job.attributes.get('failure_reason') != 'stuck_or_timeout_failure':
            return False

        return True


class DataIntegrityMatcher:  # pylint: disable=too-few-public-methods
    """Check whether a job failed with a data_integrity_failure."""

    name = 'integrity'
    description = 'Job failed with data_integrity_failure'
    action = 'retry'

    def check(self, job: CachedJob):  # pylint: disable=no-self-use
        """Check status/failure reason for a match."""
        if job.gl_job.status != 'failed':
            return False
        if job.gl_job.attributes.get('failure_reason') != 'data_integrity_failure':
            return False

        return True


class NoTraceMatcher:
    # pylint: disable=too-few-public-methods
    """Notify if a job has no trace."""

    name = 'no-trace'
    description = 'Job has no trace'
    action = 'report'

    def check(self, job: CachedJob):  # pylint: disable=no-self-use
        """
        Check that the job's trace is not empty.

        job.trace returns a string split by new lines, so when the trace
        is empty the result will be a list with an empty string.
        """
        return job.trace == ['']


class TestsNotRun:  # pylint: disable=too-few-public-methods
    """Check that all tests in a test job ran or were properly skipped."""

    name = 'tests-not-run'
    description = 'Test job has tests that did not run'
    action = 'report'

    def check(self, job: CachedJob):
        """Check that all tests ran until completion or were skipped."""
        if not job.gl_job.name.startswith('test'):
            return False

        if not (kcidb_content := job.artifact_file('kcidb_all.json', split=False)):
            return False

        with NamedTemporaryFile() as tmpfile:
            pathlib.Path(tmpfile.name).write_text(kcidb_content, encoding='utf8')
            kcidbfile = KCIDBFile(tmpfile.name)

        unrun_tests = [
            test for test in kcidbfile.data.get('tests', [])
            if not test.get('status') or misc.get_nested_key(test, 'misc/forced_skip_status')
        ]

        if unrun_tests:
            self.description = f'{len(unrun_tests)} test(s) that did not run'

        return unrun_tests


def notify(job: CachedJob, notification, notify_chat: bool):
    """Send a message to the logs and optionally the chat bot."""
    with misc.only_log_exceptions():
        status = job.gl_job.status
        name = job.gl_job.name
        job_id = job.gl_job.id
        pipeline_id = job.gl_pipeline.id
        retriggered = ' (retriggered)' if job.retriggered else ''
        job_url = job.gl_job.web_url
        pipeline_url = job.gl_pipeline.web_url
        message = (f'🤠 <{pipeline_url}|P{pipeline_id}> <{job_url}|J{job_id}>{retriggered} '
                   f'{name} {status}: {notification}')
        LOGGER.info('%s', message)
        if notify_chat:
            chatbot.send_message(message)
