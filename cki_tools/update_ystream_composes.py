"""Script to update kpet-db ystream composes."""
# pylint: disable=broad-exception-raised,broad-exception-caught
import argparse
import difflib
import json
import logging
import os
import pathlib
import re
import subprocess
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib import session
from cki_lib import yaml
from cki_lib.config_tree import process_config_tree
import sentry_sdk

LOGGER: logging.Logger = logger.get_logger('cki_tools.update_ystream_composes')
SESSION = session.get_session('cki_tools.update_ystream_composes', raise_for_status=True)


def get_distro_name(distro_name_pattern: str, tags: list[str]) -> str:
    """Query Beaker to get the latest distro."""
    cmd = [
        'bkr', 'distros-list',
        '--limit', '1',
        '--format', 'json',
        '--name', distro_name_pattern,
    ] + [f'--tag={t}' for t in tags]
    LOGGER.debug('Running: %s', cmd)
    distro_data = json.loads(subprocess.run(cmd, capture_output=True, check=True).stdout)
    if result := misc.get_nested_key(distro_data, '0/distro_name'):
        return typing.cast(str, result)
    raise Exception(f'Failed to get distro for {distro_name_pattern}')


def get_buildroot_name(distro_name: str) -> str:
    """Query CTS to find the buildroot compose of given compose."""
    compose_info = SESSION.get(url=f'{os.environ["CTS_URL"]}/api/1/composes/{distro_name}').json()
    if result := next((c for c in compose_info['children'] if c.startswith('BUILDROOT')), None):
        return typing.cast(str, result)
    raise Exception(f'Failed to get buildroot for {distro_name}')


def tree_diff(tree_path: pathlib.Path, diff_path: str, **kwargs: str) -> str:
    """Get the patch to update the compose in a kpet-db tree."""
    data = orig = pathlib.Path(tree_path).read_text(encoding='utf8')
    for name, value in kwargs.items():
        data = re.sub(f'.*set {name}.*', f'{{% set {name} = "{value}" %}}', data, re.MULTILINE)
    if diff := list(difflib.unified_diff(orig.split('\n'), data.split('\n'),
                                         fromfile=diff_path, tofile=diff_path, lineterm='')):
        return '\n'.join(diff) + '\n'
    return ''


def main(argv: typing.Optional[typing.List[str]] = None) -> int:
    """Script to update kpet-db ystream composes."""
    parser = argparse.ArgumentParser(
        description='Output the patch that would be needed to update y-stream trees in kpet-db')
    parser.add_argument('--config', default=os.environ.get('YSTREAM_COMPOSES_CONFIG'),
                        help='YAML y-stream compose configuration file to use')
    parser.add_argument('--config-path', default=os.environ.get('YSTREAM_COMPOSES_CONFIG_PATH'),
                        help='Path to YAML y-stream compose configuration file')
    args = parser.parse_args(argv)

    config = yaml.load(contents=args.config, file_path=args.config_path)

    exit_code = 0
    for tree_name, compose in process_config_tree(config).items():
        with logger.logging_env(compose):
            LOGGER.info('Processing %s', tree_name)
            try:
                distro_name = get_distro_name(compose['distro_name'], compose['tags'])
                buildroot_name = get_buildroot_name(distro_name)
                LOGGER.info('Found distro %s with buildroot %s', distro_name, buildroot_name)
                tree_path = f'trees/{tree_name}.j2'
                diff = tree_diff(pathlib.Path(tree_path), tree_path,
                                 distro_name=distro_name,
                                 buildroot_name=buildroot_name)
            except Exception:
                LOGGER.exception('Failed to update tree %s', tree_name)
                exit_code = 1
            else:
                print(diff, end='')
    return exit_code


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    sys.exit(main())
