"""Credential metrics."""

from cki_lib import misc
import prometheus_client

from cki.deployment_tools import secrets

from . import utils


def process() -> str:
    """Return token metrics."""
    registry = prometheus_client.CollectorRegistry()
    metric_token_expires_at = prometheus_client.Gauge(
        'cki_token_expires_at', 'timestamp when validity of token ends',
        ['name', 'active', 'deployed'], registry=registry)
    for token in utils.all_tokens({'bugzilla_token', 'gitlab_token'}):
        meta = secrets.secret(f'{token}#')
        active = misc.booltostr(meta.get('active', True))
        deployed = misc.booltostr(meta.get('deployed', True))
        if expires_at := meta.get('expires_at'):
            timestamp = misc.datetime_fromisoformat_tz_utc(expires_at).timestamp()
            metric_token_expires_at.labels(token, active, deployed).set(timestamp)
    return prometheus_client.generate_latest(registry).decode('utf8')
