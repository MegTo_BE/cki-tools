"""Credential management."""
import argparse
import typing

from cki_lib import logger

from . import gitlab
from . import metrics

LOGGER = logger.get_logger('cki_tools.credentials_manager')


def main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Run main loop."""
    parser = argparse.ArgumentParser(description='Process credentials')

    subparsers = parser.add_subparsers(dest='service')

    parser_gitlab = subparsers.add_parser('gitlab')
    parser_gitlab.set_defaults(func=lambda a: gitlab.process(a.action, a.token, a.dry_run, a.force))
    parser_gitlab.add_argument('action', choices=['create', 'update', 'validate', 'rotate'],
                               help='What to do')
    parser_gitlab.add_argument('--token', help='Token name for create/rotate')
    parser_gitlab.add_argument('--dry-run', action='store_true',
                               help='Do not modify secrets or create tokens during rotation')
    parser_gitlab.add_argument('--force', action='store_true',
                               help='Force token rotation even if new enough')

    parser_metrics = subparsers.add_parser('metrics')
    parser_metrics.set_defaults(func=lambda _: metrics.process())

    args = parser.parse_args(argv)
    if result := args.func(args):
        print(result.strip())


if __name__ == '__main__':
    main()
