"""GitLab credential management."""
from datetime import timedelta
import os
import sys
import typing

from cki_lib import logger
from cki_lib import misc
from cki_lib import yaml
from cki_lib.gitlab import get_instance
from cki_lib.gitlab import parse_gitlab_url
from gitlab import client
from gitlab.v4 import objects

from cki.deployment_tools import secrets

from . import utils

LOGGER = logger.get_logger(__name__)

TIMEDELTA_UNUSED = timedelta(hours=24)
TIMEDELTA_EXPIRY = timedelta(days=365)


def _create_personal_token(
    gl_instance: client.Gitlab,
    gl_owner: objects.Project | objects.Group,
    meta: dict[str, typing.Any],
) -> str:
    gl_personal_token = gl_owner.access_tokens.create({
        'name': meta['token_name'],
        'expires_at': (misc.now_tz_utc() + TIMEDELTA_EXPIRY).date().isoformat(),
        'scopes': meta['scopes'],
        'access_level': meta['access_level'],
    })
    gl_user = gl_instance.users.get(gl_personal_token.user_id)
    meta.update({
        'token_id': gl_personal_token.id,
        'created_at': gl_personal_token.created_at,
        'expires_at': gl_personal_token.expires_at,
        'revoked': gl_personal_token.revoked,
        'active': gl_personal_token.active,
        'user_id': gl_personal_token.user_id,
        'user_name': gl_user.username,
        'deployed': True,
    })
    return typing.cast(str, gl_personal_token.token)


def _create_deploy_token(
    gl_owner: objects.Project | objects.Group,
    meta: dict[str, typing.Any],
) -> str:
    gl_deploy_token = gl_owner.deploytokens.create({
        'name': meta['token_name'],
        'expires_at': meta.get('expires_at'),
        'scopes': meta['scopes'],
        # the '+' in the default gitlab+deploy-token-{n} breaks repo mirroring
        'username': misc.now_tz_utc().strftime('gitlab-deploy-token-%f'),
    })
    meta.update({
        'token_id': gl_deploy_token.id,
        'created_at': misc.now_tz_utc().isoformat(),
        'expires_at': gl_deploy_token.expires_at,
        'revoked': gl_deploy_token.revoked,
        'active': not gl_deploy_token.expired,
        'user_name': gl_deploy_token.username,
        'deployed': True,
    })
    return typing.cast(str, gl_deploy_token.token)


def create(token: str) -> None:
    """Create a GitLab token."""
    LOGGER.info('Processing %s', token)
    meta = secrets.secret(f'{token}#')
    match meta['token_type'], meta['gitlab_token_type']:
        case 'gitlab_token', 'project_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['project_url'])
            token_secret = _create_personal_token(gl_instance, gl_owner, meta)
        case 'gitlab_token', 'group_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['group_url'])
            token_secret = _create_personal_token(gl_instance, gl_owner, meta)
        case 'gitlab_token', 'project_deploy_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['project_url'])
            token_secret = _create_deploy_token(gl_owner, meta)
        case 'gitlab_token', 'group_deploy_token':
            gl_instance, gl_owner = parse_gitlab_url(meta['group_url'])
            token_secret = _create_deploy_token(gl_owner, meta)
        case token_type, gitlab_token_type:
            raise Exception(f'Unable to create {token_type}/{gitlab_token_type} {token}')
    secrets.edit(f'{token}#', meta)
    secrets.edit(f'{token}', token_secret)


def _update_project_token(meta: dict[str, str]) -> None:
    _, gl_project = parse_gitlab_url(meta['project_url'])
    gl_project_token = gl_project.access_tokens.get(meta['token_id'])
    gl_member = gl_project.members.get(gl_project_token.user_id)
    meta.update({
        'scopes': gl_project_token.scopes,
        'access_level': gl_member.access_level,
        'token_name': gl_project_token.name,
        'token_id': gl_project_token.id,
        'created_at': gl_project_token.created_at,
        'expires_at': gl_project_token.expires_at,
        'revoked': gl_project_token.revoked,
        'active': gl_project_token.active,
        'user_id': gl_project_token.user_id,
        'user_name': gl_member.username,
    })


def _update_group_token(meta: dict[str, str]) -> None:
    _, gl_group = parse_gitlab_url(meta['group_url'])
    gl_group_token = gl_group.access_tokens.get(meta['token_id'])
    gl_member = gl_group.members.get(gl_group_token.user_id)
    meta.update({
        'scopes': gl_group_token.scopes,
        'access_level': gl_member.access_level,
        'token_name': gl_group_token.name,
        'token_id': gl_group_token.id,
        'created_at': gl_group_token.created_at,
        'expires_at': gl_group_token.expires_at,
        'revoked': gl_group_token.revoked,
        'active': gl_group_token.active,
        'user_id': gl_group_token.user_id,
        'user_name': gl_member.username,
    })


def _update_personal_token(
        meta: typing.Dict[str, str],
        secrets_data: dict[str, dict[str, typing.Any]],
) -> None:
    api_token = get_api_token_for_pat(meta, {'api', 'read_api'}, secrets_data)
    gl_instance = get_instance(meta['instance_url'], token=secrets.secret(api_token))
    gl_personal_token = gl_instance.personal_access_tokens.get(meta['token_id'])
    gl_user = gl_instance.users.get(gl_personal_token.user_id)
    meta.update({
        'scopes': gl_personal_token.scopes,
        'token_name': gl_personal_token.name,
        'token_id': gl_personal_token.id,
        'created_at': gl_personal_token.created_at,
        'expires_at': gl_personal_token.expires_at,
        'revoked': gl_personal_token.revoked,
        'active': gl_personal_token.active,
        'user_id': gl_personal_token.user_id,
        'user_name': gl_user.username,
    })


def _update_project_deploy_token(meta: typing.Dict[str, typing.Any]) -> None:
    _, gl_project = parse_gitlab_url(meta['project_url'])
    gl_deploy_token = gl_project.deploytokens.get(meta['token_id'])
    meta.update({
        'scopes': gl_deploy_token.scopes,
        'token_name': gl_deploy_token.name,
        'token_id': gl_deploy_token.id,
        'expires_at': gl_deploy_token.expires_at,
        'revoked': gl_deploy_token.revoked,
        'active': not gl_deploy_token.expired,
        'user_name': gl_deploy_token.username,
    })


def _update_group_deploy_token(meta: typing.Dict[str, typing.Any]) -> None:
    _, gl_group = parse_gitlab_url(meta['group_url'])
    gl_deploy_token = gl_group.deploytokens.get(meta['token_id'])
    meta.update({
        'scopes': gl_deploy_token.scopes,
        'token_name': gl_deploy_token.name,
        'token_id': gl_deploy_token.id,
        'expires_at': gl_deploy_token.expires_at,
        'revoked': gl_deploy_token.revoked,
        'active': not gl_deploy_token.expired,
        'user_name': gl_deploy_token.username,
    })


def _update_runner_token(meta: typing.Dict[str, typing.Any], secret_token: str) -> None:
    gl_instance = get_instance(meta['instance_url'])
    response = gl_instance.http_post(
        '/runners/verify', post_data={"token": secret_token})
    meta.update({
        'token_id': response['id'],
    })
    if expires_at := response['token_expires_at']:
        meta['expires_at'] = expires_at
        meta['active'] = misc.now_tz_utc() < misc.datetime_fromisoformat_tz_utc(expires_at)
    else:
        meta['active'] = True


def update(single_token: str) -> None:
    """Update the secret meta information about GitLab tokens."""
    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    for token in misc.flattened(single_token or utils.all_tokens(['gitlab_token'])):
        LOGGER.info('Processing %s', token)
        meta = secrets.secret(f'{token}#')
        token_type = meta['gitlab_token_type']
        LOGGER.debug('Processing %s - token type: %s', token, token_type)
        match token_type:
            case 'project_token':
                _update_project_token(meta)
            case 'group_token':
                _update_group_token(meta)
            case 'personal_token':
                _update_personal_token(meta, secrets_data)
            case 'project_deploy_token':
                _update_project_deploy_token(meta)
            case 'group_deploy_token':
                _update_group_deploy_token(meta)
            case 'runner_authentication_token':
                _update_runner_token(meta, secrets.secret(token))
            case _:
                LOGGER.info('Token meta update for %s %s not supported', token_type, token)
        secrets.edit(f'{token}#', meta)


def validate(token: str) -> None:
    # pylint: disable=too-many-branches
    """Check validity of the token."""
    LOGGER.info('Processing %s', token)
    token_secret = secrets.secret(token)
    meta = secrets.secret(f'{token}#')
    token_type = meta['gitlab_token_type']
    deployed = meta.get('deployed', True)
    LOGGER.debug('Processing %s - token type: %s', token, token_type)
    match deployed, token_type:
        case False, _:
            LOGGER.info('Not checking validity of undeployed token %s', token)
        case _, 'project_token':
            gl_instance = get_instance(meta['project_url'], token=token_secret)
            gl_project_token = gl_instance.personal_access_tokens.get('self')
            if gl_project_token.revoked or not gl_project_token.active:
                raise Exception(f'Revoked token {token}')
        case _, 'group_token':
            gl_instance = get_instance(meta['group_url'], token=token_secret)
            gl_group_token = gl_instance.personal_access_tokens.get('self')
            if gl_group_token.revoked or not gl_group_token.active:
                raise Exception(f'Revoked token {token}')
        case _, 'personal_token':
            gl_instance = get_instance(meta['instance_url'], token=token_secret)
            gl_personal_token = gl_instance.personal_access_tokens.get('self')
            if gl_personal_token.revoked or not gl_personal_token.active:
                raise Exception(f'Revoked token {token}')
        case _, 'project_deploy_token':
            gl_instance, gl_project = parse_gitlab_url(meta['project_url'])
            gl_deploy_token = gl_project.deploytokens.get(meta['token_id'])
            if gl_deploy_token.revoked or gl_deploy_token.expired:
                raise Exception(f'Revoked or expired token {token}')
        case _, 'group_deploy_token':
            gl_instance, gl_group = parse_gitlab_url(meta['group_url'])
            gl_deploy_token = gl_group.deploytokens.get(meta['token_id'])
            if gl_deploy_token.revoked or gl_deploy_token.expired:
                raise Exception(f'Revoked or expired token {token}')
        case _, 'runner_authentication_token':
            gl_instance = get_instance(meta['instance_url'])
            gl_instance.http_post('/runners/verify', post_data={"token": token_secret})
        case _:
            LOGGER.info('Token validation for %s %s not supported', token_type, token)


def rotate(single_token: str, dry_run: bool, force: bool) -> None:
    """Rotate tokens."""
    if force and not dry_run and not single_token:
        print('--force requires --token', file=sys.stderr)
        return

    secrets_data = yaml.load(file_path=os.environ.get('CKI_SECRETS_FILE'))
    tokens = {k.split('/')[0] for k in misc.flattened(single_token or list(secrets_data.keys()))}
    delta = timedelta(days=30)
    for token in tokens:
        if not (metas := {
            k: v.get('meta', {})
            for k, v in secrets_data.items()
            if (k == token or k.startswith(f'{token}/'))
            and misc.get_nested_key(v, 'meta/token_type') == 'gitlab_token'
            and misc.get_nested_key(v, 'meta/gitlab_token_type') in
                {'project_token', 'group_token', 'personal_token'}
        }):
            if single_token:
                print(f'Unable to find rotatable secrets for {single_token}', file=sys.stderr)
            else:
                LOGGER.debug('Not processing %s - unable to find rotatable secrets', token)
            continue

        if next(iter(metas.values()))['gitlab_token_type'] == 'personal_token':
            rotate_personal_token(token, delta, metas, dry_run, force, secrets_data)
        else:
            rotate_project_group_token(token, delta, metas, dry_run, force)


def rotate_project_group_token(
    token: str,
    delta: timedelta,
    metas: dict[str, dict[str, typing.Any]],
    dry_run: bool,
    force: bool,
) -> None:
    """Rotate a project or group token."""
    if not force and any(
        not (expires_at := v.get('expires_at'))
        or misc.now_tz_utc() + delta < misc.datetime_fromisoformat_tz_utc(expires_at)
        for v in metas.values()
    ):
        LOGGER.debug('Not processing %s - new enough', token)
        return

    old_name = next((k for k, v in sorted(
        metas.items(), key=lambda i: misc.datetime_fromisoformat_tz_utc(i[1]['expires_at']),
        reverse=True,
    )), '')
    new_name = f'{token}/{int(misc.now_tz_utc().timestamp())}'
    new_meta = {k: v for k, v in metas[old_name].items() if k not in {
        'token_id',
        'created_at',
        'expires_at',
        'revoked',
        'active',
        'user_id',
        'user_name',
    }}

    if dry_run:
        print(f'Would recreate token {old_name} into {new_name}', file=sys.stderr)
    else:
        print(f'Recreating token {old_name} into {new_name}', file=sys.stderr)
        for secret_name, secret_meta in metas.items():
            if secret_meta.get('deployed', False):
                secrets.edit(f'{secret_name}#deployed', False)
        secrets.edit(f'{new_name}#', new_meta)
        create(new_name)


def get_api_token_for_pat(
    pat_meta: dict[str, typing.Any],
    scopes: set[str],
    secrets_data: dict[str, dict[str, typing.Any]],
) -> str:
    """Find a (preferably deployed) token with enough permissions to rotate the PAT."""
    return next(
        k for k, v in sorted(secrets_data.items(), reverse=True,
                             key=lambda i: misc.get_nested_key(i[1], 'meta/deployed', True))
        if misc.get_nested_key(v, 'meta/token_type') == pat_meta['token_type']
        and misc.get_nested_key(v, 'meta/gitlab_token_type') == pat_meta['gitlab_token_type']
        and misc.get_nested_key(v, 'meta/instance_url') == pat_meta['instance_url']
        and misc.get_nested_key(v, 'meta/user_id') == pat_meta['user_id']
        and misc.get_nested_key(v, 'meta/active', True)
        and scopes & set(misc.get_nested_key(v, 'meta/scopes', []))
    )


def rotate_personal_token(
    token: str,
    delta: timedelta,
    metas: dict[str, dict[str, typing.Any]],
    dry_run: bool,
    force: bool,
    secrets_data: dict[str, dict[str, typing.Any]],
) -> None:
    # pylint: disable=too-many-arguments,too-many-locals
    """Rotate a personal token."""
    old_name, old_meta = next((
        i for i in metas.items() if i[1].get('active', True) and not i[1].get('deployed', True)
    ), (None, {}))

    if not old_name:
        LOGGER.debug('Not processing %s - no undeployed version', token)
        return

    if not force and (
        not (expires_at := old_meta.get('expires_at')) or
        misc.now_tz_utc() + delta < misc.datetime_fromisoformat_tz_utc(expires_at)
    ):
        LOGGER.debug('Not processing %s - new enough', token)
        return

    new_name = f'{token}/{int(misc.now_tz_utc().timestamp())}'

    api_token = get_api_token_for_pat(old_meta, {'api'}, secrets_data)

    gl_instance = get_instance(old_meta['instance_url'], token=secrets.secret(api_token))
    gl_token = gl_instance.personal_access_tokens.get(old_meta['token_id'])

    if not force and gl_token.last_used_at and (
        used := (misc.now_tz_utc() - misc.datetime_fromisoformat_tz_utc(gl_token.last_used_at))
    ) < TIMEDELTA_UNUSED:
        print(f'Not rotating {old_name} as used {used} ago', file=sys.stderr)
        return

    if dry_run:
        print(f'Would rotate token {old_name} into {new_name} via {api_token}', file=sys.stderr)
    else:
        print(f'Rotating token {old_name} into {new_name} via {api_token}', file=sys.stderr)
        for secret_name, secret_meta in metas.items():
            if secret_meta.get('deployed', True):
                secrets.edit(f'{secret_name}#deployed', False)
        gl_token.rotate(expires_at=(misc.now_tz_utc() + TIMEDELTA_EXPIRY).date().isoformat())
        secrets.edit(f'{new_name}', gl_token.token)
        secrets.edit(f'{new_name}#', {**old_meta, 'deployed': True, 'token_id': gl_token.id})
        update(new_name)
        secrets.edit(f'{old_name}#active', False)
        secrets.edit(f'{old_name}#revoked', True)


def process(action: str, token: str = '', dry_run: bool = False, force: bool = False) -> None:
    """Process gitlab actions."""
    match action:
        case 'create':
            create(token)
        case 'update':
            update(token)
        case 'validate':
            for all_token in utils.all_tokens(['gitlab_token']):
                validate(all_token)
        case 'rotate':
            rotate(token, dry_run, force)
