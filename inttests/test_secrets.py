"""Tests for HV secrets processing."""
import contextlib
import os
import pathlib
import tempfile
import typing
from unittest import mock

from cki_lib import inttests
from cki_lib import yaml

from cki.deployment_tools import secrets


@inttests.skip_without_requirements()
class TestHashiCorpVaultSecrets(inttests.KubernetesIntegrationTest,
                                inttests.vault.HashiCorpVaultServer):
    """Tests for HV secrets processing."""

    @staticmethod
    @contextlib.contextmanager
    def _setup_secrets(
        data: typing.Dict[str, typing.Any],
    ) -> typing.Iterator[None]:
        secrets._read_secrets_file.cache_clear()  # pylint: disable=protected-access
        with (tempfile.TemporaryDirectory() as directory,
              mock.patch.dict(os.environ, {'CKI_SECRETS_FILE': f'{directory}/secrets.yml'})):
            pathlib.Path(directory, 'secrets.yml').write_text(yaml.dump(data), encoding='utf8')
            yield

    def test_smoke(self) -> None:
        """Basic smoke tests."""
        with self._setup_secrets({'foo': {'backend': 'hv'}}):
            secrets.edit('foo', 'bar')
            self.assertEqual(secrets.secret('foo'), 'bar')
            self.assertEqual(secrets.vault_list(), ['foo'])
