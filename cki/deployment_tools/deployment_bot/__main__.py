"""Deployment bot CLI interface."""
import argparse
import os
import sys

from cki_lib import config_tree
from cki_lib import misc
from cki_lib import yaml
import sentry_sdk

from . import deployment


def main(args):
    """CLI Interface."""
    parser = argparse.ArgumentParser(
        description='Listen to deployment events and trigger deployment pipelines')
    parser.add_argument('--config-path',
                        default=os.environ.get('DEPLOYMENT_BOT_CONFIG_PATH', 'config.yml'),
                        help='Path to the config file')
    parser.add_argument('--message',
                        help='Webhook message body for a manual deployment')
    parsed_args = parser.parse_args(args)

    config = config_tree.process_config_tree(yaml.load(
        contents=os.environ.get('DEPLOYMENT_BOT_CONFIG'),
        file_path=parsed_args.config_path))

    if parsed_args.message:
        deployment.Deployment(config).callback(body=yaml.load(contents=parsed_args.message))
    else:
        deployment.Deployment(config).listen()


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    main(sys.argv[1:])
