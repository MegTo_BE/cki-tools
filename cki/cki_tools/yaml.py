"""YAML CLI tools for miscellaneous operations not supported by shyaml."""
import argparse
import re
import sys
import typing

from cki_lib import misc
from cki_lib import yaml
import sentry_sdk


def split_dot(data: str) -> typing.List[str]:
    """Split string on dots, but ignore backslash+dot."""
    return list(t.replace(r'\.', '.') for t in re.split(r'(?<!\\)\.', data))


def set_value(data: typing.Any, key_path: str, value: typing.Any) -> None:
    """Implement the set-value command."""
    target = data
    keys = split_dot(key_path)
    key: typing.Union[str, int]
    for index, key in enumerate(keys):
        if isinstance(target, list):
            key = int(key)
        if index < len(keys) - 1:
            target = target[key]
        else:
            target[key] = yaml.load(contents=value)


def delete(data: typing.Any, key_path: str) -> None:
    """Implement the del command."""
    target = data
    keys = split_dot(key_path)
    key: typing.Union[str, int]
    for index, key in enumerate(keys):
        if isinstance(target, list):
            key = int(key)
        if index < len(keys) - 1:
            target = target[key]
        else:
            del target[key]


def main(args: typing.Sequence[str]) -> None:
    """Run CLI YAML tool."""
    parser = argparse.ArgumentParser(description='Miscellaneous YAML operations')

    parser.add_argument('action', choices=('set-value', 'del'), help='Action to perform')
    parser.add_argument('key', help='Target for the action')
    parser.add_argument('value', nargs='?', help='YAML-formatted value')
    parsed_args = parser.parse_args(args)

    data = yaml.load(contents=sys.stdin.read())

    if parsed_args.action == 'set-value':
        set_value(data, parsed_args.key, parsed_args.value)
    elif parsed_args.action == 'del':
        delete(data, parsed_args.key)
    else:
        raise Exception(f'Unknown action {parsed_args.action}')

    yaml.dump(data, stream=sys.stdout)


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    main(sys.argv[1:])
