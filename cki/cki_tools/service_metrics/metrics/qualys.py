"""Qualys reports."""
import csv
import datetime
import email
import io
import os
import pathlib
import poplib
import typing

import boto3
from cki_lib import s3bucket
from cki_lib import yaml
from cki_lib.cronjob import CronJob
from cki_lib.logger import get_logger
from dateutil import parser
import prometheus_client

LOGGER = get_logger(__name__)
QUALYS_CONFIG = yaml.load(contents=os.environ.get('QUALYS_CONFIG', ''))

DEFAULT_DATETIME = datetime.datetime(2000, 1, 1, tzinfo=datetime.timezone.utc)


class QualysMetrics(CronJob):
    """Calculate Qualys report metrics."""

    schedule = '5 0 * * *'  # once a day

    metric_reports = prometheus_client.Gauge(
        'cki_qualys_reports',
        'Vulnerabilities reported by Qualys',
        ['host', 'type']
    )

    @staticmethod
    def _upload_s3(date: datetime.date, raw: bytes) -> None:
        spec = s3bucket.parse_bucket_spec(os.environ[QUALYS_CONFIG['bucket']])
        LOGGER.debug('Uploading to %s/%s', spec.bucket, spec.prefix)
        client = boto3.Session().client('s3',
                                        aws_access_key_id=spec.access_key or None,
                                        aws_secret_access_key=spec.secret_key or None,
                                        endpoint_url=spec.endpoint or None)
        client.put_object(Bucket=spec.bucket,
                          Key=f'{spec.prefix}raw/{date.isoformat()}.csv',
                          Body=raw)

    @staticmethod
    def _upload_s3_parsed(date: datetime.date, raw: bytes) -> None:
        """Return a properly formated csv file."""
        reader = csv.reader(io.StringIO(raw.decode('utf8'), newline=''))
        writer = csv.writer(parsed_body := io.StringIO(newline=''))
        if not (header := next((row for row in reader if 'DNS' in row), None)):
            raise Exception('Unable to find CSV header')
        writer.writerow(header + ['Last Updated'])
        writer.writerows([row + [date.isoformat()] for row in reader if row])
        parsed = parsed_body.getvalue().encode('utf8')

        spec = s3bucket.parse_bucket_spec(os.environ[QUALYS_CONFIG['bucket']])
        LOGGER.debug('Uploading to %s/%s', spec.bucket, spec.prefix)
        client = boto3.Session().client('s3',
                                        aws_access_key_id=spec.access_key or None,
                                        aws_secret_access_key=spec.secret_key or None,
                                        endpoint_url=spec.endpoint or None)
        client.put_object(Bucket=spec.bucket, Key=f'{spec.prefix}data.csv', Body=parsed)

    @staticmethod
    def _download_s3_latest() -> typing.Tuple[datetime.date, bytes]:
        spec = s3bucket.parse_bucket_spec(os.environ[QUALYS_CONFIG['bucket']])
        LOGGER.debug('Downloading from %s/%s', spec.bucket, spec.prefix)
        client = boto3.Session().client('s3',
                                        aws_access_key_id=spec.access_key or None,
                                        aws_secret_access_key=spec.secret_key or None,
                                        endpoint_url=spec.endpoint or None)
        key = client.list_objects_v2(
            Bucket=spec.bucket, Prefix=f'{spec.prefix}raw/')['Contents'][-1]['Key']
        date = parser.parse(pathlib.Path(key).stem, default=DEFAULT_DATETIME).date()
        body = client.get_object(Bucket=spec.bucket, Key=key)['Body'].read()
        return date, body

    @staticmethod
    def _download_s3_parsed() -> typing.List[typing.Dict[str, str]]:
        spec = s3bucket.parse_bucket_spec(os.environ[QUALYS_CONFIG['bucket']])
        LOGGER.debug('Downloading from %s/%s', spec.bucket, spec.prefix)
        client = boto3.Session().client('s3',
                                        aws_access_key_id=spec.access_key or None,
                                        aws_secret_access_key=spec.secret_key or None,
                                        endpoint_url=spec.endpoint or None)
        body = client.get_object(Bucket=spec.bucket, Key=f'{spec.prefix}data.csv')['Body'].read()
        return list(csv.DictReader(io.StringIO(body.decode('utf8'), newline='')))

    @staticmethod
    def _download_pop3() -> typing.Iterator[typing.Tuple[datetime.date, bytes]]:
        pop3 = QUALYS_CONFIG['pop3']
        mailbox = poplib.POP3_SSL(pop3['host'], pop3['port'])
        mailbox.user(pop3['user'])
        mailbox.pass_(os.environ[pop3['pass']])
        for mail in mailbox.list()[1]:
            LOGGER.debug('Downloading %s', mail)
            mesg_num, octets = (int(i) for i in mail.split())
            msg = email.message_from_bytes(b'\n'.join(mailbox.top(mesg_num, octets)[1]))
            if csv_part := next((p for p in msg.walk() if
                                 p.get_content_disposition() == 'attachment' and
                                 p.get_content_type() == 'application/octet-stream' and
                                 'csv' in p.get_filename()), None):

                date = email.utils.parsedate_to_datetime(
                    msg['Date']).astimezone(datetime.timezone.utc).date()
                raw = csv_part.get_payload(decode=True)
                yield date, raw
            mailbox.dele(mesg_num)
        mailbox.quit()

    def update_qualys_reports(self) -> None:
        """Update Qualys reports."""
        for date, raw in self._download_pop3():
            self._upload_s3(date, raw)

    def update_qualys_parsed_report(self) -> None:
        """Update Qualys reports."""
        date, raw = self._download_s3_latest()
        self._upload_s3_parsed(date, raw)

    def update_qualys_metrics(self) -> None:
        """Update Qualys metrics."""
        stale_threshold = datetime.timedelta(days=QUALYS_CONFIG.get('stale_threshold', 2))
        grace_threshold = datetime.timedelta(days=QUALYS_CONFIG.get('grace_threshold', 7))
        now = datetime.datetime.now(datetime.timezone.utc)
        hosts: typing.Dict[str, typing.Dict[str, int]] = {}
        for row in self._download_s3_parsed():
            host = row['DNS']
            hosts.setdefault(host, {'fixed': 0, 'grace': 0, 'active': 0, 'total': 0})
            first_detected = parser.parse(row['First Detected'], default=DEFAULT_DATETIME)
            last_detected = parser.parse(row['Last Detected'], default=DEFAULT_DATETIME)
            last_updated = parser.parse(row['Last Updated'], default=DEFAULT_DATETIME)
            # Old report, don't generate metrics so monitoring can pick it up.
            if now - last_updated > stale_threshold:
                continue
            # The vulnerability was explicitely detected as not active anymore.
            # Impossible to say whether the host is still there, so assume it is.
            if row['Vuln Status'] != 'Active':
                hosts[host]['total'] += 1
                hosts[host]['fixed'] += 1
                continue
            # The vulnerability is active, but it was not detected again in the
            # last scan. Assume that the host went away before it could be
            # fixed/detected as fixed and don't generate metrics for it.
            if now - last_detected > stale_threshold:
                continue
            # Younger than the grace threshold, so don't alert just yet
            if last_detected - first_detected < grace_threshold:
                hosts[host]['total'] += 1
                hosts[host]['grace'] += 1
                continue
            # These now are the only vulnerabilities we really care about:
            # Qualys report is up-to-date, they are active, still detected,
            # and should have been fixed by the regular updates/reboots
            hosts[host]['total'] += 1
            hosts[host]['active'] += 1
        for host, reports in hosts.items():
            for report_type, report_count in reports.items():
                self.metric_reports.labels(host, report_type).set(report_count)

    def run(self, **_: typing.Any) -> None:
        """Download and and analyze Qualys metrics."""
        self.update_qualys_reports()
        self.update_qualys_parsed_report()
        self.update_qualys_metrics()
