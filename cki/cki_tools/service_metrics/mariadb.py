"""MariaDB Connection Handler."""
import functools
import os
import typing

import mariadb
import prometheus_client as prometheus

METRIC_TIME = prometheus.Histogram(
    'cki_mariadb_query_duration_seconds', 'Time spent handling a query',
    ['hostname', 'query'],
)


class MariaDBHandler:
    """MariaDB connection handler."""

    def __init__(self, *, host=None, port=None, user=None, password=None, database=None):
        # pylint: disable=too-many-arguments
        """Initialize MariaDB connection."""
        host = host or os.environ['MARIADB_HOST']
        port = port or int(os.environ['MARIADB_PORT'])
        user = user or os.environ['MARIADB_USER']
        password = password or os.environ['MARIADB_PASSWORD']
        database = database or os.environ['MARIADB_DATABASE']

        self.connection_params = {
            'host': host,
            'port': port,
            'user': user,
            'password': password,
            'database': database,
            'reconnect': True,
            'ssl': True,
            'autocommit': True,
        }

    def escape_tuple(self, vals: typing.Iterable[str]) -> str:
        """Properly escape an SQL tuple. Needs a DB connection."""
        return ''.join((
            '(',
            ','.join("'" + self.connection.escape_string(v) + "'" for v in vals),
            ')',
        ))

    @functools.cached_property
    def connection(self):
        """Get DB connection. Split from init for simpler testing."""
        return mariadb.connect(**self.connection_params)

    def execute(self, query, *args):
        """Execute a given query."""
        with METRIC_TIME.labels(
                self.connection_params['host'], ' '.join(query.split())
        ).time(), self.connection.cursor() as cursor:
            cursor.execute(query, *args)
            return cursor.fetchall()
