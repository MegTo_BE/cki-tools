#!/usr/bin/python3
"""Manage Dogtag application certificates."""

import argparse
import collections
import datetime
import os
import pathlib
import typing
from urllib import parse

from OpenSSL import crypto
from boto3.session import Session
from cki_lib import misc
from cki_lib import yaml
from cki_lib.logger import get_logger
from pki.cert import CertClient
from pki.client import PKIConnection
import sentry_sdk

LOGGER = get_logger('cki.cki_tools.refresh_dogtag_certificates')

Certificate = collections.namedtuple(
    'Certificate', 'cert_id not_valid_before not_valid_after user')


class S3File:
    """File on S3.

    This duplicates functionality of cki_lib/s3bucket.py to explore a simpler
    interface to AWS credentials. Basically, use a URL like
    https://endpoint/bucket/path for S3 access, and rely on the environment to
    be setup correctly to access it.
    """

    def __init__(self, s3_url: str) -> None:
        """Create a custom boto session for S3.

        This will use credentials available in the environment.

        s3_url: https://endpoint/bucket/path
        """
        parts = parse.urlparse(s3_url)
        bucket, bucket_path = parts.path[1:].split('/', maxsplit=1)
        resource = Session().resource('s3', endpoint_url=f'{parts.scheme}://{parts.netloc}')
        self._s3_object = resource.Object(bucket, bucket_path)

    def download(self) -> bytes:
        """Download a single file from S3."""
        return self._s3_object.get()['Body'].read()

    def upload(self, content: bytes) -> None:
        """Upload a single file to S3."""
        self._s3_object.put(Body=content)


class DogtagCertificateManager:
    """Manage Dogtag application certificates."""

    def __init__(self, server: str, name: str) -> None:
        """Manage a Dogtag application certificate."""
        parts = parse.urlparse(server)
        self.conn = CertClient(PKIConnection(parts.scheme, parts.hostname, str(parts.port)))

        self.user = os.environ[name + '_USER']
        self.password = os.environ[name + '_PASSWORD']
        self.key = crypto.load_privatekey(crypto.FILETYPE_PEM, os.environ[name + '_PRIVATE_KEY'])

    @staticmethod
    def from_environment() -> typing.List[str]:
        """Return all private key names available in the environment."""
        return [s.replace('_PRIVATE_KEY', '') for s in os.environ if s.endswith('_PRIVATE_KEY')]

    def authorize(self, request: typing.Any) -> None:
        """Add authorization attributes to a Dogtag API request."""
        request.Attributes['Attribute'] += [
            {'name': 'uid', 'value': self.user},
            {'name': 'pwd', 'value': self.password},
        ]

    def create_csr(self) -> str:
        """Return a new PEM-formatted CSR."""
        req = crypto.X509Req()
        req.set_pubkey(self.key)
        req.sign(self.key, 'sha256')
        return crypto.dump_certificate_request(crypto.FILETYPE_PEM, req).decode('ascii')

    def request_cert(self) -> None:
        """Request a new certificate."""
        enrollment_request = self.conn.create_enrollment_request('caDirAppUserCert', {
            'cert_request_type': 'pkcs10',
            'cert_request': self.create_csr(),
        })
        self.authorize(enrollment_request)
        self.conn.submit_enrollment_request(enrollment_request)

    @staticmethod
    def _datetime(timestamp: str) -> datetime.datetime:
        return datetime.datetime.fromtimestamp(float(timestamp) / 1000, tz=datetime.timezone.utc)

    def list_valid_certs(self) -> typing.List[Certificate]:
        """Return currently valid certificates."""
        return list(Certificate(
            c.serial_number,
            self._datetime(c.not_valid_before),
            self._datetime(c.not_valid_after),
            self.user,
        ) for c in self.conn.list_certs(user_id=self.user, match_exactly=True,
                                        status='VALID', max_results=100))

    def get_cert(self, cert_id) -> str:
        """Return the PEM-encoded certificate."""
        return self.conn.get_cert(cert_id).encoded.replace('\r', '')


def retrieve_certificates(
    dogtag_url: str,
) -> typing.Dict[str, typing.List[Certificate]]:
    """Get all certificates available from the server."""
    all_certs = {}
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    for name in DogtagCertificateManager.from_environment():
        manager = DogtagCertificateManager(dogtag_url, name)
        all_certs[name] = manager.list_valid_certs()
        for cert in all_certs[name]:
            LOGGER.info('Valid certificate for %s (%s): %s - %s/%s days old/to go',
                        name, manager.user, cert.cert_id,
                        (now - cert.not_valid_before).days, (cert.not_valid_after - now).days)
    return all_certs


def refresh_certificates(
    dogtag_url: str,
    refresh_after: datetime.timedelta,
    refresh_before: datetime.timedelta,
    all_certs: typing.Dict[str, typing.List[Certificate]],
) -> None:
    """Refresh certificates."""
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    for name in all_certs:
        certs = all_certs[name]
        if not certs or \
           (refresh_after and all(c.not_valid_before + refresh_after < now for c in certs)) or \
           (refresh_before and all(c.not_valid_after - refresh_before < now for c in certs)):
            if not misc.is_production():
                LOGGER.info('Would refresh certificate for %s if in production', name)
            else:
                print(f'Refreshing certificate for {name}')
                manager = DogtagCertificateManager(dogtag_url, name)
                manager.request_cert()
                all_certs[name] = manager.list_valid_certs()


def revoke_certificates(
    expire_after: datetime.timedelta,
    expire_before: datetime.timedelta,
    all_certs: typing.Dict[str, typing.List[Certificate]],
) -> None:
    """Propose to revoke certificates."""
    now = datetime.datetime.now(tz=datetime.timezone.utc)
    for name in all_certs:
        # ignore certificate that has the latest expiry date
        certs = sorted(all_certs[name], key=lambda c: c.not_valid_after, reverse=True)[1:]
        for cert in certs:
            if (expire_after and cert.not_valid_before + expire_after < now) or \
               (expire_before and cert.not_valid_after - expire_before < now):
                LOGGER.warning('Certificate can be revoked: %s - %s', cert.cert_id, cert.user)


def update_newest_certificates(
    local_certs: typing.Dict[str, str],
    dogtag_url: str,
    all_certs: typing.Dict[str, typing.List[Certificate]],
) -> None:
    """Export certificates with latest expiry date."""
    for name in all_certs:
        manager = DogtagCertificateManager(dogtag_url, name)
        certs = sorted(all_certs[name], key=lambda c: c.not_valid_after, reverse=True)
        if certs:
            local_certs[name + '_CERTIFICATE'] = manager.get_cert(certs[0].cert_id)


def main(argv: typing.Optional[typing.List[str]] = None) -> None:
    """Run the main CLI."""
    parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser.add_argument('--dogtag-url', default=os.environ.get('DOGTAG_URL'),
                        help='Dogtag server to use')
    parser.add_argument('--refresh-after', default='0', type=misc.parse_timedelta,
                        help='Refresh certificates after they are so old')
    parser.add_argument('--refresh-before', default='0', type=misc.parse_timedelta,
                        help='Refresh certificates before they expire')
    parser.add_argument('--expire-after', default='0', type=misc.parse_timedelta,
                        help='Propose to expire certificates after they are so old')
    parser.add_argument('--expire-before', default='0', type=misc.parse_timedelta,
                        help='Propose to expire certificates before they expire')
    parser.add_argument('--s3-url', default=os.environ.get('DOGTAG_S3_URL'),
                        help='S3 file path like https://endpoint/bucket/path.yml')
    parser.add_argument('--download-only', metavar='FILE',
                        help='Only download from S3 and fail otherwise')
    args = parser.parse_args(argv)

    if args.download_only:
        pathlib.Path(args.download_only).write_bytes(S3File(args.s3_url).download())
        return

    # download certificates
    local_certs = {}
    if args.s3_url:
        try:
            local_certs = yaml.load(contents=S3File(args.s3_url).download())
        # pylint: disable=broad-except
        except Exception:
            LOGGER.info('Unable to download current certificates from %s', args.s3_url)

    # get all certificates available from the server
    all_certs = retrieve_certificates(args.dogtag_url)

    # refresh certificates
    refresh_certificates(args.dogtag_url, args.refresh_after, args.refresh_before, all_certs)

    # propose to revoke certificates
    revoke_certificates(args.expire_after, args.expire_before, all_certs)

    # export certificates with latest expiry date
    update_newest_certificates(local_certs, args.dogtag_url, all_certs)

    # upload newest certificates
    if args.s3_url:
        S3File(args.s3_url).upload(yaml.dump(local_certs, encoding='utf8'))


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    main()
