"""AMQP Bridge."""
import os

from cki_lib import metrics
from cki_lib import misc
from cki_lib import yaml
import sentry_sdk

from cki.cki_tools import amqp_bridge


def run():
    """Run the selected listener."""
    metrics.prometheus_init()

    config = yaml.load(contents=os.environ['AMQP_BRIDGE_CONFIG'])

    if config['protocol'] == 'amqp091':
        amqp_bridge.process_amqp091(config)
    elif config['protocol'] == 'amqp10':
        amqp_bridge.process_amqp10(config)
    else:
        raise Exception("Configuration not handled.")


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    run()
