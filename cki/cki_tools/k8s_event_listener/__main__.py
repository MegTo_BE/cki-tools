"""Listen to k8s events and print them to stdout."""
from cki_lib import metrics
from cki_lib import misc
import sentry_sdk

from cki.cki_tools import k8s_event_listener


def run():
    """Run the selected listener."""
    metrics.prometheus_init()

    k8s_event_listener.EventListener().run()


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    run()
