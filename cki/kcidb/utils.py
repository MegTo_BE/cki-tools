"""Misc utility functions for kcidb."""
import hashlib
import re

from cki_lib.session import get_session

SESSION = get_session(__name__)


def patch_list_hash(patch_list):
    """Get hash of a list of patches."""
    if not patch_list:
        return ''

    patch_hash_list = []
    for patch_url in patch_list:
        # Transform /mbox/ url into /raw/ to get the patch diff only.
        # Patchwork mbox includes headers that can change after people reply to the patches.
        patch = SESSION.get(re.sub(r'/mbox/?$', '/raw/', patch_url))
        patch_hash_list.append(
            hashlib.sha256(patch.content).hexdigest()
        )

    merged_hashes = '\n'.join(patch_hash_list) + '\n'
    return hashlib.sha256(merged_hashes.encode('utf8')).hexdigest()


def _get_regressed_test_ids(dw_all) -> set:
    """Get the set of test IDs that have been triaged with a regression.

    Args:
        dw_all: Instance of the response from the "checkout-all" endpoint.

    Returns:
        Set of test IDs that have been triaged with a regression.
    """
    testresult_id_2_test_id = {tr.id: tr.test_id for tr in dw_all.testresults}
    return {
        i.test_id or testresult_id_2_test_id[i.testresult_id]
        for i in dw_all.issueoccurrences
        if i.is_regression and (i.test_id or i.testresult_id)
    }


def _get_triaged_test_ids(dw_all) -> set:
    """Get the set of test IDs that have been successfully triaged (excluding regressions).

    Args:
        dw_all: Instance of the response from the "checkout-all" endpoint.

    Returns:
        Set of test IDs that have been successfully triaged, and don't pose a regression.
    """
    filtered_occurs = [i for i in dw_all.issueoccurrences if not i.is_regression]

    # NOTE: After DW#321 we'll be able to rely just on the test_id
    triaged_test_ids = {i.test_id for i in filtered_occurs if i.test_id and not i.testresult_id}

    # NOTE: After DW#321 this will change dramatically
    testresult_id_2_test_id = {tr.id: tr.test_id for tr in dw_all.testresults}
    triaged_testresults = (i.testresult_id for i in filtered_occurs if i.testresult_id)
    triaged_through_results = {testresult_id_2_test_id[tr_id] for tr_id in triaged_testresults}

    return triaged_test_ids | triaged_through_results


def unknown_issues(dw_all, tests=None):
    """Get the list of tests that need to be reported.

    Args:
        dw_all: Instance of the response from the "checkout-all" endpoint.
        tests: Optional list of tests to filter. Defaults to `dw_all.tests`.

    Returns:
        List of tests missing triage or with regression.
    """
    if tests is None:
        tests = dw_all.tests

    triageable_test_ids = {
        test.id for test in tests if test.status in {"ERROR", "FAIL"} and not test.waived
    }

    triaged_test_ids = _get_triaged_test_ids(dw_all)
    regressed_test_ids = _get_regressed_test_ids(dw_all)

    problematic_test_ids = (
        (triageable_test_ids - triaged_test_ids)  # Tests missing triage
        | (triageable_test_ids & regressed_test_ids)  # Tests triaged with regression
    )

    test_lookup = {t.id: t for t in dw_all.tests}
    return [test_lookup[t] for t in problematic_test_ids]
