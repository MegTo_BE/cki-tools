"""Listen for UMB messages and submit them to Datawarehouse."""

from http import HTTPStatus
import itertools
import os
import typing

from cki_lib import logger
from cki_lib import messagequeue
from cki_lib import metrics
from cki_lib import misc
from cki_lib import session
from cki_lib.kcidb import ValidationError
from cki_lib.kcidb import validate_extended_kcidb_schema
import datawarehouse
import prometheus_client as prometheus
from requests.exceptions import HTTPError
import sentry_sdk

LOGGER = logger.get_logger('cki.kcidb.datawarehouse_umb_submitter')
SESSION = session.get_session('cki.kcidb.datawarehouse_umb_submitter', timeout=600)


METRIC_UMB_RESULTS = prometheus.Counter(
    'umb_results', 'Number of UMB results submitted to datawarehouse')
METRIC_UMB_DATA_SIZE = prometheus.Histogram(
    "umb_data_size", "Size of UMB results submitted to datawarehouse"
)

BATCH_SIZE = 100


def batched(iterable, n) -> typing.Iterable[tuple]:
    """Batch data from the iterable into tuples of length n. The last batch may be shorter.

    Backport of Python 3.12.
    """
    if n < 1:
        raise ValueError("n must be at least one")
    it = iter(iterable)
    while batch := tuple(itertools.islice(it, n)):
        yield batch


def batch_payload(data: dict) -> typing.Iterable[dict]:
    """Yield the KCIDB payload batching the items if they exceed the size threshold."""
    checkouts = data.setdefault("checkouts", [])
    builds = data.setdefault("builds", [])
    tests = data.setdefault("tests", [])

    data_size = len(tests) + len(builds) + len(checkouts)
    METRIC_UMB_DATA_SIZE.observe(data_size)

    # Should not submit payload with empty lists
    if data_size == 0:
        return

    # If the data is small enough, we don't need to split it
    if data_size < BATCH_SIZE:
        yield data
        return

    for object_type in ('checkouts', 'builds', 'tests'):
        for batch in batched(data[object_type], BATCH_SIZE):
            yield {
                "version": data["version"],
                object_type: batch,
            }


class Receiver:
    # pylint: disable=too-few-public-methods
    """Provides callback to submit to Datawarehouse."""

    def __init__(self):
        """Initialize Datawarehouse object used for every callback."""
        self.dw_api = datawarehouse.Datawarehouse(os.environ['DATAWAREHOUSE_URL'],
                                                  os.environ['DATAWAREHOUSE_TOKEN_SUBMITTER'],
                                                  session=SESSION)

    def callback(self, body, **_):
        """Submit a UMB result message to datawarehouse."""
        try:
            validate_extended_kcidb_schema(body)
        except ValidationError:
            LOGGER.exception("Failed to validate data from UMB")
            return

        try:
            for batched_body in batch_payload(body):
                self.dw_api.kcidb.submit.create(data=batched_body)
        except HTTPError as error:
            if error.response.status_code == HTTPStatus.BAD_REQUEST:
                LOGGER.exception("Failed to submit data from UMB to DataWarehouse")
            else:
                raise
        else:
            METRIC_UMB_RESULTS.inc()


def main():
    """Submit UMB results to datawarehouse."""
    metrics.prometheus_init()
    receiver = Receiver()

    connection = messagequeue.MessageQueue()
    connection.consume_messages(
        os.environ.get('WEBHOOK_RECEIVER_EXCHANGE', 'cki.exchange.webhooks'),
        os.environ['RABBITMQ_ROUTING_KEYS'].split(),
        receiver.callback,
        queue_name=os.environ['RABBITMQ_QUEUE']
    )


if __name__ == '__main__':
    misc.sentry_init(sentry_sdk)
    main()
